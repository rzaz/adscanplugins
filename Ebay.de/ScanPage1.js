﻿//////////////////////////////////////////////
//$SNIPPET: ScanPage1
//////////////////////////////////////////////
$("html").on("submit", function (ev) {
    console.log("submit page 1");
    var elements = [];
    
    $("a.is-active").each(function () {
        var element = {
            "Tagname": $(this).prop("tagName"),
            "Parent": null,
            "Value": $(this).val(),
            "Attributes": [{
                "Name": "id",
                "Value": $(this).prop("id")
            }]
        }
        elements.push(element);
    });
   
    adscan.ScanPage(JSON.stringify(elements));
    return true;
});