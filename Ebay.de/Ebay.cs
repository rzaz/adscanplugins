﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Core;
using Core.NavigationAsync;
using Ebay.de.Properties;
using NLog;
using PluginFrameWork;
using PluginFrameWork.Ads;
using Attribute = PluginFrameWork.Ads.Attribute;

namespace Ebay.de
{
    public class Plugin : IPluginAsync, IPluginTemplateAsync
        {
        private Navigation _helper;
        private Helpers _ebayHelper;
        private CancellationToken _cancelToken;
        private Ad _ad;
        private readonly Logger _log = LogManager.GetCurrentClassLogger();
        private string _md5File = "";

        #region "IPluginAsync"
        Dictionary<string, string> images = new Dictionary<string, string>();
        private TaskCompletionSource<bool> _tcsImageUploaded;

        public string Name { get { return "Ebay.de"; } }
        public CancellationToken CancelToken { set { _cancelToken = value; } }

        public void Init(object webview)
        {
            _helper = new Navigation(webview, _cancelToken);
            _ebayHelper = new Helpers(webview, _cancelToken);
            _helper.AutoInjectScripts();
        }

        public async Task<bool> LoginAsync(Account account)
        {
            _helper.Navigate("http://kleinanzeigen.ebay.de/anzeigen/m-abmelden.html");
            await _helper.WaitForFinishAsync();
            _helper.Navigate("https://kleinanzeigen.ebay.de/anzeigen/m-einloggen.html");
            await _helper.WaitForFinishAsync();

            await _ebayHelper.SetTextBoxAsync(Helpers.CreateSelector("login-email"), account.Name);
            await _ebayHelper.SetTextBoxAsync(Helpers.CreateSelector("login-password"), account.Password);
            await _ebayHelper.ClickAsync(Helpers.CreateSelector("login-submit"));
            await _helper.WaitForFinishAsync(); //wachten tot we naar "uw advertenties" worden herleid
           return true;

        }

        public async Task<bool> FillAsync(Account account, Ad ad, bool catchExceptions)
        {
            _helper.Navigate("https://kleinanzeigen.ebay.de/anzeigen/p-anzeige-aufgeben.html");
            await _helper.WaitForFinishAsync("aufgeben.html");
            await TaskEx.Delay(200, _cancelToken);

            images.Clear();

            // eerste pagina 

            var linkitems = from Element x in ad.FindPage("page1").Elements where x.Tagname.Equals("A") select x;

            foreach (var linkitem in linkitems)
            {
                var selector = Helpers.CreateSelector(linkitem.FindAttribute("id").Value);
                await _ebayHelper.ClickAsync(selector);
                _log.Debug("linkitem " + linkitem.FindAttribute("id").Value);
                await TaskEx.Delay(1);
            }

            //weiter...und snell
            await _ebayHelper.ClickAsync("$('button:submit.btn-blue')");

            // tweede pagina
            await _helper.WaitForFinishAsync();


            //select boxes
            var selects = from Element x in ad.FindPage("page2").Elements where x.Tagname.Equals("SELECT") select x;

            _log.Debug("selects");
            foreach (var selectbox in selects)
            {
                var controlId = selectbox.FindAttribute("id").Value;
                _log.Trace(controlId);
                _log.Trace("SetSelectboxAsync");
                // de id's hebben een . in de naam ! 
                var selector = Helpers.CreateSelector("SELECT", "id", controlId);
                await _ebayHelper.SetSelectboxAsync(selector, selectbox.Value).CatchException(catchExceptions);
            }

            // radio's
            _cancelToken.ThrowIfCancellationRequested();
            _log.Debug("radios");

            foreach (var radioButton in Helpers.GetRadiosFromElements(ad.FindPage("page2").Elements))
            {
                await
                    _ebayHelper.ClickRadioAsync(Helpers.CreateSelector("INPUT", "id",
                        radioButton.FindAttribute("id").Value)).CatchException(catchExceptions);
                _log.Debug("radio " + radioButton.FindAttribute("id").Value);
                await TaskEx.Delay(1);
            }

            //checkboxen
            _cancelToken.ThrowIfCancellationRequested();
            _log.Debug("checkboxen");
            //vul alle checkboxes in
            foreach (var checkBox in Helpers.GetCheckboxesFromElements(ad.FindPage("page1").Elements))
            {
                if (checkBox.Value != "True" && checkBox.Value != "False") checkBox.Value = "False";
                await _ebayHelper.ClickCheckboxAsync(Helpers.CreateSelector(checkBox.FindAttribute("id").Value)).TimeoutAfter(1000).CatchException(catchExceptions);
                _log.Debug("checkbox " + checkBox.FindAttribute("id").Value);
                await TaskEx.Delay(1);
            }

            //textboxen
            _cancelToken.ThrowIfCancellationRequested();
            _log.Debug("textboxen");
            foreach (var textBox in Helpers.GetTextBoxesFromElements(ad.FindPage("page2").Elements))
            {
                if (!String.IsNullOrEmpty(textBox.Value))
                {
                    await
                        _ebayHelper.SetTextBoxAsync(Helpers.CreateSelector(textBox.FindAttribute("id").Value),
                            textBox.Value.Replace("\\", "\\\\").Replace("'", "\\'")).TimeoutAfter(1000).CatchException(catchExceptions);
                    _log.Debug("textbox " + textBox.FindAttribute("id").Value);
                    await TaskEx.Delay(1);
                }
            }

            //textarea
            var areas = from Element x in ad.FindPage("page2").Elements where x.Tagname.Equals("TEXTAREA") select x;
            _log.Debug("text areas");
            foreach (var area in areas)
            {
                var id = area.FindAttribute("id").Value;
                var areatext = area.Value;
                var htmlConverter = new HtmlConverter();
                var keeplist = new List<string> { "UL", "LI" };

                var replacelist = new List<Translate> { new Translate("P", "P", false), new Translate("BR", "BR", false) };
                {
                    htmlConverter.TagsToKeep = keeplist;
                    htmlConverter.ReplaceList = replacelist;
                    htmlConverter.HmtlString = areatext;
                    htmlConverter.IgnoreStyles = true;
                    htmlConverter.convert();
                }

                string html = htmlConverter.ConvertedHtml;
                html = html.HtmlDecode();
                html = html.Replace("\\", "\\\\");
                html = html.Replace("\r", "");
                html = html.Replace("\n", "\\n");
                html = html.Replace("'", "\\'");

                //todo settextarea async?
                _ebayHelper.InvokeScriptString(string.Format("$(\"#{0}\").val('{1}')", id, html.TakeFirst(4000)));
            }

            // images
            await UploadImagesInAd(ad).TimeoutAfter(60000);

            return true;
        }

        public async Task<bool> ConfirmAdAsync(Ad ad, bool manualClick)
        {
            if (!manualClick)
            {
                _helper.InvokeScriptString("$(\"#pstad-submit\").click();");
            }
            else
            {
                await _helper.InvokeScriptStringAsync("$(\"#pstad-submit span.btn-label\").html('Plaats')", true);
                await _ebayHelper.ScrollIntoFocusAsync("$(\"#pstad-submit\")");
                _helper.InvokeScriptString(String.Format("$(\"#pstad-submit\").fadeTo({0}, 0.5).fadeTo({1}, 1.0).fadeTo({0}, 0.5).fadeTo({1}, 1.0).fadeTo({0}, 0.5).fadeTo({1}, 1.0).fadeTo({0}, 0.5).fadeTo({1}, 1.0).fadeTo({0}, 0.5).fadeTo({1}, 1.0)", 1500, 500));

            }
            await _helper.WaitForFinishAsync();

            //fouten ?
            if (_helper.InvokeScriptString("$(\"span.formerror\").length") != "0")
                throw new Exception("Form Error");

            // is dat ad succesvol geplaatst?
            // ad is gelukt als de box met id checking-done de class  is-hidden niet heeft...
            var test = _helper.InvokeScriptString("$(\"#checking-done\").not(\".is-hidden\")".Trim());
            _log.Trace(!string.IsNullOrEmpty(test));
            await TaskEx.Delay(5000, _cancelToken);
            
            // scheise!!
            // wat is het unieke id van deze ad ? (nodig bij deleten)
            var adurl = _helper.InvokeScriptString("$(\"#criteo-conversion\").prop('src');".Trim());
            var adid = Regex.Match(adurl, "(?<=i1%3D)[0-9]+").ToString();
            
            _ad.LastPlacedURL = adid; // store ad id
            _ad.LastPlaced = DateTime.Now;
            return true;
        }

        public async Task<bool> RecordAsync(Account account, Ad ad)
        {
            _helper.Navigate("https://kleinanzeigen.ebay.de/anzeigen/p-anzeige-aufgeben.html");
            await _helper.WaitForFinishAsync("aufgeben.html");
            await TaskEx.Delay(200, _cancelToken);

            //pagina 1 : rubriek keuze

            var elements = await ScanPage1();
            ad.Pages.Clear();
            ad.Pages.Add(new Page());
            ad.Pages.Last().Name = "page1";
            ad.Pages.Last().Url = _helper.InvokeScriptString("document.URL");
            ad.Pages.Last().Elements.AddRange(elements);
            ad.Account = account;

            //pagina 2 : de advertentie
            await _helper.WaitForFinishAsync(); //wachten op nog een redirect die ze doen anders schieten we het script op een verkeerde pagina in

            DetectImageChanges(); 

            _ebayHelper.TrackManualImageUploadsBegin(md5 => { _md5File = md5; }, "gif");
            elements = await ScanPage2();

            //todo Wat nu als je iets vergeten bent in te vullen zoals de titel???

            ad.Pages.Add(new Page());
            ad.Pages.Last().Name = "page2";
            ad.Pages.Last().Url = _helper.InvokeScriptString("document.URL");
            ad.Pages.Last().Elements.AddRange(elements);
            ad.Account = account;

            _ebayHelper.TrackManualImageUploadsEnd();
            _ebayHelper.CleanTemporaryImages();
            
            images.Clear();
            return true;
        }

        public async Task<DeleteResult> DeleteAsync(Account account, Ad ad, bool guessMode)
        {
            string adId = ad.LastPlacedURL;

            if (String.IsNullOrEmpty(adId))
            {
                return DeleteResult.NotFound;
            }

            _log.Debug("advertentie id " + adId);
            _helper.Navigate("http://kleinanzeigen.ebay.de/anzeigen/m-meine-anzeigen.html");
            await _helper.WaitForFinishAsync("meine-anzeigen.html");

            // wacht op zoek box, vul in en druk enter
            await TaskEx.Delay(2000);
            await _ebayHelper.WaitUntilExistsAsync("$(\"#site-search-query\")");
            await _helper.InvokeScriptStringAsync(String.Format("$(\"#site-search-query\").val('{0}')", adId), true);
            await _helper.InvokeScriptStringAsync("$(\"#site-search-submit\").click()", true);
            await _helper.WaitForFinishAsync();

            // zijn er resultaten?
            //$("div.adminbox")  //aanwezig?
            const string selector = "$(\"div.adminbox\").length";
            int a = Int32.Parse(_helper.InvokeScriptString(selector));
            if (a != 0)
            {
                //ja
                //click lochen link  $("#pvap-mngad-dltad")
                await _ebayHelper.ClickAsync(Helpers.CreateSelector("pvap-mngad-dltad"));
                //click zeker weten $("#modal-delete-ad-sbmt")
                await _ebayHelper.WaitUntilExistsAsync("$(\"#modal-delete-ad-sbmt\")");
                await _ebayHelper.ClickAsync(Helpers.CreateSelector("modal-delete-ad-sbmt"));
                
                // komt nog een scherm met een divje erin 
                await _helper.WaitForFinishAsync();
                // $("div.successbox.clearfix p").html();
                var test = _helper.InvokeScriptString("$(\"div.successbox.clearfix p\").html()".Trim());

                // staat daar iets van  wurde gelöscht?
                if (test.Contains("wurde gelöscht"))
                {
                    // dan gelukt
                    _log.Debug("DELETE AD: advertentie is verwijderd");
                    return DeleteResult.Deleted;
                }
                return DeleteResult.Failed;
            }
            return DeleteResult.NotFound;
        }

        public async Task<bool> EditAsync(Account account, Ad ad, Action removeSplash)
        {
            await FillAsync(account, ad, true);
            removeSplash();

            _ebayHelper.TrackManualImageUploadsBegin(md5 => { _md5File = md5; }, "gif");
            var elements = await ScanPage2();

            ad.Pages.Remove(ad.Pages.Last()); // remove old page 2
            ad.Pages.Add(new Page());
            ad.Pages.Last().Name = "page2";
            ad.Pages.Last().Url = _helper.InvokeScriptString("document.URL");
            ad.Pages.Last().Elements.AddRange(elements);
            ad.Account = account;
            _ebayHelper.TrackManualImageUploadsEnd();
            _ebayHelper.CleanTemporaryImages();
            return true;
        }

        public PluginStatus Status { get; set; }
        #endregion

        
        private Element FindElement(string tagName, string attributeName, string lookForAttribute)
        {
            Element tempElement;
            if (string.IsNullOrEmpty(attributeName))
            {
                tempElement = (from x in _ad.FindPage("page2").Elements
                               where !string.IsNullOrEmpty(x.Tagname) && x.Tagname.Equals(tagName)
                               select x).SingleOrDefault();

            }
            else
            {
                List<Element> allElementsWithAttributeName = (from x in _ad.FindPage("page2").Elements where x.FindAttribute(attributeName) != null select x).ToList();
                tempElement = (from x in allElementsWithAttributeName where x.FindAttribute(attributeName).Value.Equals(lookForAttribute) select x).SingleOrDefault();
            }

            if (tempElement != null)
            {
                return tempElement;
            }
            var el = new Element { Tagname = tagName };
            el.Attributes.Add(new Attribute());
            el.Attributes.Last().Name = attributeName;
            el.Attributes.Last().Value = lookForAttribute;
            el.Attributes.Add(new Attribute());
            el.Attributes.Last().Name = "type";
            el.Attributes.Last().Value = "file";
            _ad.Pages.Last().Elements.Add(el);
            return el;
        }
        
        private async Task<List<Element>> ScanPage1()
        {
            var tcsPageHook = new TaskCompletionSource<List<Element>>();
            _helper.CreateJavascriptCallback("ScanPage", (method, args) =>
            {
                _log.Debug("Entering scanroutine");
                var test = _ebayHelper.JSonToObject<List<Element>>(args[0].ToString());
                tcsPageHook.SetResult(test);
                return "";
            });

            await _helper.InvokeScriptStringAsync(
               _helper.GetJavascript(Resources.ScanPage1, "ScanPage1"), true);

            var allElements = await tcsPageHook.Task;
            return allElements;
        }
        private async Task<List<Element>> ScanPage2()
        {

            var tcsPageHook = new TaskCompletionSource<List<Element>>();

            _cancelToken.Register(tcsPageHook.SetCanceled);

            _helper.CreateJavascriptCallback("ScanPage", (method, args) =>
            {
                _log.Debug("Entering scanroutine");
                var test = _ebayHelper.JSonToObject<List<Element>>(args[0].ToString());
                tcsPageHook.SetResult(test);
                return "";
            });

            await _helper.InvokeScriptStringAsync(
                _helper.GetJavascript(Resources.ScanPage2, "ScanPage2"), true);

            var allElements = await tcsPageHook.Task;

            _cancelToken.ThrowIfCancellationRequested();

            foreach (var image in images)
            {
                var element = new Element { Tagname = "image" };
                element.Attributes.Add(new Attribute { Name = "id", Value = image.Key });
                element.Attributes.Add(new Attribute { Name = "type", Value = "file" });
                element.Attributes.Add(new Attribute { Name = "name", Value = image.Value });
                element.Value = image.Value;
                allElements.Add(element);
            }

            return allElements;
        }

        private void DetectImageChanges()
        {
            _tcsImageUploaded = new TaskCompletionSource<bool>();

            _helper.CreateJavascriptCallback("imageUploaded", (method, args) =>
            {
                _log.Debug("Entering scanroutine");
                images.Add(args[0].ToString(), _md5File);
                _tcsImageUploaded.TrySetResult(true);
                _tcsImageUploaded = new TaskCompletionSource<bool>();
                return "";
            });

            _helper.CreateJavascriptCallback("imageDelete", (method, args) =>
            {
                _log.Debug("Entering  image delete routine");
                _log.Debug(args[0].ToString());
                images.Remove(args[0].ToString());
                return "";
            });

            _helper.InvokeScriptStringAsync(
                _helper.GetJavascript(Resources.ImageScripts, "UploadFinished"), false);
        }

        private async Task<Boolean> UploadImagesInAd(Ad ad)
        {
            DetectImageChanges();

            var imagelist = (from x in ad.FindPage("page2").Elements
                             where (x.Tagname.Equals("image", StringComparison.InvariantCultureIgnoreCase)
                             && !x.Value.Equals("0") && !String.IsNullOrEmpty(x.Value))
                             select new { MD5 = x.Value, id = x.FindAttribute("id").Value });

            foreach (var image in imagelist)
            {
                _md5File = image.MD5;
                await _ebayHelper.UploadImage(string.Format("$(\"input:file\").last()"), image.MD5, null, "gif", _tcsImageUploaded.Task);
                await TaskEx.Delay(1000, _cancelToken);
            }
            return true;
        }
        
        //??????
        string IPluginTemplateAsync.BareAdName
        {
            get { throw new NotImplementedException(); }
        }
        //??????
        string IPluginTemplateAsync.Description
        {
            get { return FindElement("textarea", "id", "pstad-descrptn").Value; }
            set { FindElement("textarea", "id", "pstad-descrptn").Value = value; }
        }

        string IPluginTemplateAsync.GetImage(int number)
        {
            return FindElement("image", "id", number.ToString(CultureInfo.InvariantCulture)).Value;
        }

        string IPluginTemplateAsync.Price
        {
            get { return FindElement("INPUT", "id", "pstad-price").Value; }
            set { FindElement("INPUT", "id", "pstad-price").Value = value; }
        }

        void IPluginTemplateAsync.SetAd(Ad ad)
        {
            _ad = ad;
        }

        void IPluginTemplateAsync.SetImage(int number, string value)
        {
            FindElement("image", "id", number.ToString()).Value = value;
        }

        public string Extra { get; set; }

        string IPluginTemplateAsync.Title
        {
            get { return FindElement("INPUT", "id", "postad-title").Value; }
            set { FindElement("INPUT", "id", "postad-title").Value = value; }
        }

        


        }
}
