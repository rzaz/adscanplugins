﻿//////////////////////////////////////////////
//$SNIPPET: UploadFinished
//////////////////////////////////////////////

var ImageDebug = true;
var ImgSelector = "#pstad-pictureupload img";
var TrashSelector = "a.pictureupload-thumbnails-remove";
var FileSelector = "input:file";
var ContainerSelector = "#pstad-pictureupload";

function SetParentID(obj, id) {
    obj.parent().parent().attr("image_id", id);
}

function GetParentID(obj) {
    return obj.parent().attr("image_id");
}

// in principe is onderstaande code generiek voor alle plugins :-) geen garanties natuurlijk

var ImageId = 0;
var NeededImages = 0;
var Uploading = false;
var ImageNumbers = [0, 0, 0, 0, 0, 0, 0, 0];

function GetImageNr() {
    for (var i = 0; i < ImageNumbers.length; i++) {
        if (ImageNumbers[i] == 0) {
            return i + 1;
        }
    }
    return 0;
};

function SetImageNr(number) {
    // set image number occupied
    ImageNumbers[number - 1] = 1;
    return 0;
};

function RemoveImageNr(number) {
    // free image number
    ImageNumbers[number - 1] = 0;
    return 0;
};



// image upload click
$("body").on("click", FileSelector, function (ev) {
    if (Uploading) {
        ev.preventDefault();
        ev.stopPropagation();
        return false;
    };
    Uploading = true;
    ImageId = GetImageNr();
    NeededImages = $(ImgSelector).length + 1;

    if (ImageDebug) console.log('imageID: ' + ImageId);
    return true;
});

// image container change
$(ContainerSelector).change(function () {
    if (Uploading) {
        if (ImageDebug) console.log('imageupload change neededpics :' + NeededImages);
        if (ImageDebug) console.log('current pics :' + $(ImgSelector).length);

        window.checker = setInterval(function () {
            if ($(ImgSelector).length == NeededImages) {
                clearInterval(window.checker);
                SetImageNr(ImageId);
                SetParentID($(ImgSelector).last(), ImageId); //store image number in parent
                if (Uploading) {
                   adscan.imageUploaded(ImageId);
                }
                Uploading = false;
                if (ImageDebug) console.log('image with id ' + ImageId + " uploaded");
            };
        }, 200);
    }
});

//image delete 
$("body").on("click", TrashSelector, function () {
    var imageNumberToDelete = GetParentID($(this));
    RemoveImageNr(imageNumberToDelete);
    adscan.imageDelete(ImageNumberToDelete);
    if (ImageDebug) console.log("image delete : " + imageNumberToDelete);
});
