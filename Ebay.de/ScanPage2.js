﻿//////////////////////////////////////////////
//$SNIPPET: ScanPage2
//////////////////////////////////////////////

$("#pstad-submit span.btn-label").html('Ascan Save');

$("#adForm table.feature-table").remove();

$("#pstad-frmprview").remove();

$("#adForm").submit(function (ev) {
    var elements = [];

    $("#adForm select").each(function () {
        var element = {
            "Tagname": $(this).prop("tagName"),
            "Parent": null,
            "Value": $(this).val(),
            "Attributes": [{
                "Name": "id",
                "Value": $(this).prop("id")
            }, {
                "Name": "type",
                "Value": $(this).prop("type")
            }, {
                "Name": "name",
                "Value": $(this).prop("name")
            }, {
                "Name": "text",
                "Value": $(this).children("option:selected").text().trim()
            }]
        }
        elements.push(element);
    })

    $("#adForm input:text").not(":hidden").each(function () {
        var element = {
            "Tagname": $(this).prop("tagName"),
            "Parent": null,
            "Value": $(this).val(),
            "Attributes": [{
                "Name": "id",
                "Value": $(this).prop("id")
            }, {
                "Name": "type",
                "Value": $(this).prop("type")
            }, {
                "Name": "name",
                "Value": $(this).prop("name")
            }]
        }
        elements.push(element);
    })

    $("#adForm input:radio:checked").not(":hidden").each(function () {
        var element = {
            "Tagname": $(this).prop("tagName"),
            "Parent": null,
            "Value": 1,
            "Attributes": [{
                "Name": "id",
                "Value": $(this).prop("id")
            }, {
                "Name": "type",
                "Value": $(this).prop("type")
            }, {
                "Name": "name",
                "Value": $(this).prop("name")
            }]
        }
        elements.push(element);
    })

    $("#adForm input:checkbox").not(":hidden").each(function () {
        var element = {
            "Tagname": $(this).prop("tagName"),
            "Parent": null,
            "Value": $(this).prop("checked"),
            "Attributes": [{
                "Name": "id",
                "Value": $(this).prop("id")
            }, {
                "Name": "type",
                "Value": $(this).prop("type")
            }, {
                "Name": "name",
                "Value": $(this).prop("name")
            }]
        }
        elements.push(element);
    });

    $("#adForm textarea").not(":hidden").each(function () {
        var element = {
            "Tagname": $(this).prop("tagName"),
            "Parent": null,
            "Value": $(this).val(),
            "Attributes": [{
                "Name": "id",
                "Value": $(this).prop("id")
            }, {
                "Name": "type",
                "Value": $(this).prop("type")
            }, {
                "Name": "name",
                "Value": $(this).prop("name")
            }]
        }
        elements.push(element);
    });
    
    adscan.ScanPage(JSON.stringify(elements));
    ev.preventDefault();
    ev.stopPropagation();
    return false;
});