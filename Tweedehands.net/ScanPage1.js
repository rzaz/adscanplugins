﻿//////////////////////////////////////////////
//$SNIPPET: ScanPage1
//////////////////////////////////////////////

$("#versturen").val('Ascan Save');

$("#placead").submit(function (ev) {
    var elements = [];


    // $("#hr_chzn li.active-result.result-selected")
    // $("#sr_chzn li.active-result.result-selected")

    $("#placead select").each(function () {
        var element = {
            "Tagname": $(this).prop("tagName"),
            "Parent": null,
            "Value": $(this).val(),
            "Attributes": [{
                "Name": "id",
                "Value": $(this).prop("id")
            }, {
                "Name": "type",
                "Value": $(this).prop("type")
            }, {
                "Name": "name",
                "Value": $(this).prop("name")
            }, {
                "Name": "text",
                "Value": $(this).children("option:selected").text().trim()
            }]
        }
        elements.push(element);
    })

    $("#placead input:text").not(":hidden").each(function () {
        var element = {
            "Tagname": $(this).prop("tagName"),
            "Parent": null,
            "Value": $(this).val(),
            "Attributes": [{
                "Name": "id",
                "Value": $(this).prop("id")
            }, {
                "Name": "type",
                "Value": $(this).prop("type")
            }, {
                "Name": "name",
                "Value": $(this).prop("name")
            }]
        }
        elements.push(element);
    })

    $("#placead input:radio:checked").not(":hidden").each(function () {
        var element = {
            "Tagname": $(this).prop("tagName"),
            "Parent": null,
            "Value": 1,
            "Attributes": [{
                "Name": "id",
                "Value": $(this).prop("id")
            }, {
                "Name": "type",
                "Value": $(this).prop("type")
            }, {
                "Name": "name",
                "Value": $(this).prop("name")
            }]
        }
        elements.push(element);
    })

    $("#placead input:checkbox").not(":hidden").each(function () {
        var element = {
            "Tagname": $(this).prop("tagName"),
            "Parent": null,
            "Value": $(this).prop("checked"),
            "Attributes": [{
                "Name": "id",
                "Value": $(this).prop("id")
            }, {
                "Name": "type",
                "Value": $(this).prop("type")
            }, {
                "Name": "name",
                "Value": $(this).prop("name")
            }]
        }
        elements.push(element);
    });

    var element = {
        "Tagname": "tinymce",
        "Parent": null,
        "Value": tinyMCE.activeEditor.getContent(),
    }
    elements.push(element);

    adscan.ScanPage(JSON.stringify(elements));
    ev.preventDefault();
    ev.stopPropagation();
    return false;
});