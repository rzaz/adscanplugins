﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core;
using Core.NavigationAsync;
using NLog;
using PluginFrameWork;
using PluginFrameWork.Ads;
using Tweedehands.net.Properties;
using Attribute = PluginFrameWork.Ads.Attribute;

namespace Tweedehands.net
{
    public class Plugin : IPluginAsync, IPluginTemplateAsync
    {
        private Navigation _helper;
        private Helpers _tweedehandsHelper;
        private CancellationToken _cancelToken;
        private Ad _ad;
        private readonly Logger _log = LogManager.GetCurrentClassLogger();
        private string _md5File = "";

        #region "IPluginAsync"
        Dictionary<string, string> images = new Dictionary<string, string>();
        private TaskCompletionSource<bool> _tcsImageUploaded;


        public string Name { get { return "Tweedehands.net"; } }
        public CancellationToken CancelToken { set { _cancelToken = value; } }

        public void Init(object webview)
        {
            _helper = new Navigation(webview, _cancelToken);
            _tweedehandsHelper = new Helpers(webview, _cancelToken);
            //_helper.NoConflictModeJquery = false;
            //_helper.AutoInjectJquery();
            _helper.AutoInjectScripts();
        }

        public async Task<bool> LoginAsync(Account account)
        {
            _helper.Navigate("http://www.tweedehands.net/loguit.php");
            await _helper.WaitForFinishAsync();
            _helper.Navigate("http://www.tweedehands.net/login.php");
            await _helper.WaitForFinishAsync();

            await _tweedehandsHelper.WaitUntilExistsAsync(Helpers.CreateSelector("usrname"));
            await _tweedehandsHelper.SetTextBoxAsync(Helpers.CreateSelector("usrname"), account.Name);
            await _tweedehandsHelper.SetTextBoxAsync(Helpers.CreateSelector("passwd"), account.Password);
            await _tweedehandsHelper.ClickAsync("$(\"button:submit\")");
            await _helper.WaitForFinishAsync(); //wachten tot we naar "uw advertenties" worden herleid

            _helper.Navigate("http://www.tweedehands.net/mijnadvertentielijst.php");
            await _helper.WaitForFinishAsync();


            return true;
        }

        public async Task<bool> RecordAsync(Account account, Ad ad)
        {
            _helper.Navigate("http://www.tweedehands.net/plaats_advertentie.php");
            await _helper.WaitForFinishAsync("plaats_advertentie");
            await TaskEx.Delay(200, _cancelToken);

            images.Clear();

            //pagina 1 
            var elements = await ScanPage1();
            ad.Pages.Clear();
            ad.Pages.Add(new Page());
            ad.Pages.Last().Name = "page1";
            ad.Pages.Last().Url = _helper.InvokeScriptString("document.URL");
            ad.Pages.Last().Elements.AddRange(elements);
            ad.Account = account;
            
            _tweedehandsHelper.TrackManualImageUploadsEnd();
            _tweedehandsHelper.CleanTemporaryImages();

            return true;
        }

        public async Task<bool> FillAsync(Account account, Ad ad, bool catchExceptions)
        {
            await TaskEx.Delay(1, _cancelToken);
            return true;

        }
        
        public async Task<bool> ConfirmAdAsync(Ad ad, bool manualClick)
        {
            await TaskEx.Delay(1, _cancelToken);
            return true;
        }
        
        public async Task<DeleteResult> DeleteAsync(Account account, Ad ad, bool guessMode)
        {
            await TaskEx.Delay(1, _cancelToken);
            return DeleteResult.Deleted;
        }

        public async Task<bool> EditAsync(Account account, Ad ad, Action removeSplash)
        {
            await TaskEx.Delay(1, _cancelToken);
            return true;
        }

        public PluginStatus Status { get; set; }
        #endregion

        #region "IPluginTemplateAsync"

        public void SetAd(Ad ad)
        {
            _ad = ad;
        }

        public string Title
        {
            get { return FindElement("INPUT", "id", "titel").Value; }
            set { FindElement("INPUT", "id", "titel").Value = value; }
        }
        public string Description
        {
            get { return FindElement("tinymce", "", "").Value; }
            set { FindElement("tinymce", "", "").Value = value; }
        }
        public string Price
        {
            get { return FindElement("INPUT", "id", "prijs").Value; }
            set { FindElement("INPUT", "id", "prijs").Value = value; }
        }

        public string GetImage(int number)
        {
            return FindElement("image", "id", number.ToString()).Value;
        }

        public void SetImage(int number, string value)
        {
            FindElement("image", "id", number.ToString()).Value = value;
        }

        public string Extra { get; set; }

        public string BareAdName { get { return Title; } }

        private Element FindElement(string tagName, string attributeName, string lookForAttribute)
        {
            Element tempElement;
            if (string.IsNullOrEmpty(attributeName))
            {
                tempElement = (from x in _ad.FindPage("page1").Elements
                               where !string.IsNullOrEmpty(x.Tagname) && x.Tagname.Equals(tagName)
                               select x).SingleOrDefault();

            }
            else
            {
                List<Element> allElementsWithAttributeName = (from x in _ad.FindPage("page1").Elements where x.FindAttribute(attributeName) != null select x).ToList();
                tempElement = (from x in allElementsWithAttributeName where x.FindAttribute(attributeName).Value.Equals(lookForAttribute) select x).SingleOrDefault();
            }

            if (tempElement != null)
            {
                return tempElement;
            }
            var el = new Element { Tagname = tagName };
            el.Attributes.Add(new Attribute());
            el.Attributes.Last().Name = attributeName;
            el.Attributes.Last().Value = lookForAttribute;
            el.Attributes.Add(new Attribute());
            el.Attributes.Last().Name = "type";
            el.Attributes.Last().Value = "file";
            _ad.Pages.Last().Elements.Add(el);
            return el;
        }
        #endregion

        private async Task<List<Element>> ScanPage1()
        {
            var tcsPageHook = new TaskCompletionSource<List<Element>>();
            _helper.CreateJavascriptCallback("ScanPage", (method, args) =>
            {
                _log.Debug("Entering scanroutine");
                var test = _tweedehandsHelper.JSonToObject<List<Element>>(args[0].ToString());
                tcsPageHook.SetResult(test);
                return "";
            });

            await _helper.InvokeScriptStringAsync(
               _helper.GetJavascript(Resources.ScanPage1, "ScanPage1"), true);

            var allElements = await tcsPageHook.Task;
            return allElements;
        }
        
        private async Task<Boolean> UploadImagesInAd(Ad ad)
        {
            DetectImageChanges();

            var imagelist = (from x in ad.FindPage("page2").Elements
                             where (x.Tagname.Equals("image", StringComparison.InvariantCultureIgnoreCase)
                             && !x.Value.Equals("0") && !String.IsNullOrEmpty(x.Value))
                             select new { MD5 = x.Value, id = x.FindAttribute("id").Value });

            foreach (var image in imagelist)
            {
                _md5File = image.MD5;
                await _tweedehandsHelper.UploadImage(string.Format("$(\"input:file\").last()"), image.MD5, null, "gif", _tcsImageUploaded.Task);
                await TaskEx.Delay(1000, _cancelToken);
            }
            return true;
        }
        
        private void DetectImageChanges()
        {
            _tcsImageUploaded = new TaskCompletionSource<bool>();

            _helper.CreateJavascriptCallback("imageUploaded", (method, args) =>
            {
                _log.Debug("Entering scanroutine");
                images.Add(args[0].ToString(), _md5File);
                _tcsImageUploaded.TrySetResult(true);
                _tcsImageUploaded = new TaskCompletionSource<bool>();
                return "";
            });

            _helper.CreateJavascriptCallback("imageDelete", (method, args) =>
            {
                _log.Debug("Entering  image delete routine");
                _log.Debug(args[0].ToString());
                images.Remove(args[0].ToString());
                return "";
            });

            _helper.InvokeScriptStringAsync(
                _helper.GetJavascript(Resources.ImageScripts, "UploadFinished"), true);
        }


    }
}
