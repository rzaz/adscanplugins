﻿//////////////////////////////////////////////
//$SNIPPET: UploadFinished
//////////////////////////////////////////////

var ImageId = 0;
var NeededImages = 0;
var Uploading = false;
var ImageNumbers = [0, 0, 0, 0, 0, 0, 0, 0];

function GetImageNr() {
    for (var i = 0; i < ImageNumbers.length; i++) {
        if (ImageNumbers[i] == 0) {
            return i + 1;
        }
    }
    return 0;
};

function RemoveImageNr(number) {
    ImageNumbers[number - 1] = 0;
    return 0;
};

$("input:file").on("click", function (ev) {
    if (Uploading) {
        ev.preventDefault();
        ev.stopPropagation();
        return false;
    };
    Uploading = true;
    ImageId = GetImageNr();
    NeededImages = $("#image_list img").length + 1;

    console.log('imageID: ' + ImageId);
    return true;
});

$("#pluploadContainer").change(function () {

    console.log('imageupload change neededpics :' + NeededImages);
    console.log('current pics :' + $("#image_list img").length);

    window.checker = setInterval(function () {
        if ($("#image_list img").length == NeededImages) {
            clearInterval(window.checker);
            ImageNumbers[ImageId - 1] = 1; 
            $("#image_list div").last().attr("image_id", ImageId); 
            
            if (Uploading) {
                //adscan.imageUploaded(ImageId);
            }
            Uploading = false;
            console.log('image with id ' + ImageId + " uploaded");
        };
    }, 200);
});

$("#image_list ").on("click", "a.imgdelete", function () {
    var imageNumberToDelete = $(this).parent().attr("image_id");
    RemoveImageNr(imageNumberToDelete);
    //adscan.imageDelete(ImageNumberToDelete);
    console.log("image delete : " + imageNumberToDelete);
});
