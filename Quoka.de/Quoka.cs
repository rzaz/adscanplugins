﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core;
using Core.NavigationAsync;
using NLog;
using PluginFrameWork;
using PluginFrameWork.Ads;
using Quoka.Properties;
using Attribute = PluginFrameWork.Ads.Attribute;

namespace Quoka
{
    public class Plugin : IPluginAsync, IPluginTemplateAsync
    {
        private Navigation _helper;
        private Helpers _QuokaHelper;
        private CancellationToken _cancelToken;
        private Ad _ad;
        private readonly Logger _log = LogManager.GetCurrentClassLogger();
        private string _md5File = "";

        #region "IPluginAsync"
        Dictionary<string, string> images = new Dictionary<string, string>();
        private TaskCompletionSource<bool> _tcsImageUploaded;

        public string Name { get { return "Quoka.de"; } }
        public CancellationToken CancelToken { set { _cancelToken = value; } }

        public void Init(object webview)
        {
            _helper = new Navigation(webview, _cancelToken);
            _QuokaHelper = new Helpers(webview, _cancelToken);
            _helper = new Navigation(webview, _cancelToken);
            
            _helper.NoConflictModeJquery = true;
            _helper.AutoInjectJquery();

            _helper.AutoInjectScripts();
        }

        public async Task<bool> LoginAsync(Account account)
        {
            _helper.Navigate("http://www.quoka.de/mein-konto/logout.html");
            await _helper.WaitForFinishAsync();
            _helper.Navigate("http://www.quoka.de/mein-konto/login.html");
            await _helper.WaitForFinishAsync();

            await _QuokaHelper.SetTextBoxAsync("jq('#loginname')", account.Name);
            await _QuokaHelper.SetTextBoxAsync("jq('#password')", account.Password);
            await _QuokaHelper.ClickAsync("jq('button.button.blue.sml')");

            await _helper.WaitForFinishAsync(); //wachten tot we naar "uw advertenties" worden herleid
            return true;
        }

        public async Task<bool> FillAsync(Account account, Ad ad, bool catchExceptions)
        {
            await TaskEx.Delay(1, _cancelToken);
            return true;
        }


        public async Task<bool> ConfirmAdAsync(Ad ad, bool manualClick)
        {
            await TaskEx.Delay(1, _cancelToken);
            return true;
        }

        public async Task<bool> RecordAsync(Account account, Ad ad)
        {
            _helper.Navigate("http://www.quoka.de/inserieren/anzeige-aufgeben.html");
            await _helper.WaitForFinishAsync("aufgeben.html");
            await TaskEx.Delay(200, _cancelToken);

            //pagina 1 : er is maar 1 pagina
            //DetectImageChanges();
            _QuokaHelper.TrackManualImageUploadsBegin(md5 => { _md5File = md5; }, "gif");

            var elements = await ScanPage1();
            ad.Pages.Clear();
            ad.Pages.Add(new Page());
            ad.Pages.Last().Name = "page1";
            ad.Pages.Last().Url = _helper.InvokeScriptString("document.URL");
            ad.Pages.Last().Elements.AddRange(elements);
            ad.Account = account;

            _QuokaHelper.TrackManualImageUploadsEnd();
            _QuokaHelper.CleanTemporaryImages();

            images.Clear();
            return true;
        }



        public async Task<DeleteResult> DeleteAsync(Account account, Ad ad, bool guessMode)
        {
            await TaskEx.Delay(1, _cancelToken);
            return DeleteResult.Deleted;
        }

        public async Task<bool> EditAsync(Account account, Ad ad, Action removeSplash)
        {
            await TaskEx.Delay(1, _cancelToken);
            return true;
        }

        public PluginStatus Status { get; set; }
        #endregion

        #region "IPluginTemplateAsync"
        private Element FindElement(string tagName, string attributeName, string lookForAttribute)
        {
            Element tempElement;
            if (string.IsNullOrEmpty(attributeName))
            {
                tempElement = (from x in _ad.FindPage("page1").Elements
                               where !string.IsNullOrEmpty(x.Tagname) && x.Tagname.Equals(tagName)
                               select x).SingleOrDefault();

            }
            else
            {
                List<Element> allElementsWithAttributeName = (from x in _ad.FindPage("page1").Elements where x.FindAttribute(attributeName) != null select x).ToList();
                tempElement = (from x in allElementsWithAttributeName where x.FindAttribute(attributeName).Value.Equals(lookForAttribute) select x).SingleOrDefault();
            }

            if (tempElement != null)
            {
                return tempElement;
            }
            var el = new Element { Tagname = tagName };
            el.Attributes.Add(new Attribute());
            el.Attributes.Last().Name = attributeName;
            el.Attributes.Last().Value = lookForAttribute;
            el.Attributes.Add(new Attribute());
            el.Attributes.Last().Name = "type";
            el.Attributes.Last().Value = "file";
            _ad.Pages.Last().Elements.Add(el);
            return el;
        }
  
        string IPluginTemplateAsync.BareAdName
        {
            get { throw new NotImplementedException(); }
        }
        string IPluginTemplateAsync.Extra
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        
        string IPluginTemplateAsync.Description
        {
            get { return FindElement("textarea", "id", "text").Value; }
            set { FindElement("textarea", "id", "text").Value = value; }
        }
        string IPluginTemplateAsync.GetImage(int number)
        {
            return FindElement("image", "id", number.ToString(CultureInfo.InvariantCulture)).Value;
        }
        string IPluginTemplateAsync.Price
        {
            get { return FindElement("INPUT", "id", "adprice").Value; }
            set { FindElement("INPUT", "id", "adprice").Value = value; }
        }
        void IPluginTemplateAsync.SetAd(Ad ad)
        {
            _ad = ad;
        }
        void IPluginTemplateAsync.SetImage(int number, string value)
        {
            FindElement("image", "id", number.ToString()).Value = value;
        }
        string IPluginTemplateAsync.Title
        {
            get { return FindElement("INPUT", "id", "headline").Value; }
            set { FindElement("INPUT", "id", "headline").Value = value; }
        }
        #endregion


        private async Task<List<Element>> ScanPage1()
        {
            var tcsPageHook = new TaskCompletionSource<List<Element>>();
            _helper.CreateJavascriptCallback("ScanPage", (method, args) =>
            {
                _log.Debug("Entering scanroutine");
                var test = _QuokaHelper.JSonToObject<List<Element>>(args[0].ToString());
                tcsPageHook.SetResult(test);
                return "";
            });

            await _helper.InvokeScriptStringAsync(
               _helper.GetJavascript(Resources.ScanPage1, "ScanPage1"), true);

            var allElements = await tcsPageHook.Task;
            return allElements;
        }
        private void DetectImageChanges()
        {
            _tcsImageUploaded = new TaskCompletionSource<bool>();

            _helper.CreateJavascriptCallback("imageUploaded", (method, args) =>
            {
                _log.Debug("Entering scanroutine");
                images.Add(args[0].ToString(), _md5File);
                _tcsImageUploaded.TrySetResult(true);
                _tcsImageUploaded = new TaskCompletionSource<bool>();
                return "";
            });

            _helper.CreateJavascriptCallback("imageDelete", (method, args) =>
            {
                _log.Debug("Entering  image delete routine");
                _log.Debug(args[0].ToString());
                images.Remove(args[0].ToString());
                return "";
            });

            _helper.InvokeScriptStringAsync(
                _helper.GetJavascript(Resources.ImageScripts, "UploadFinished"), false);
        }

        private async Task<Boolean> UploadImagesInAd(Ad ad)
        {
            DetectImageChanges();

            var imagelist = (from x in ad.FindPage("page2").Elements
                             where (x.Tagname.Equals("image", StringComparison.InvariantCultureIgnoreCase)
                             && !x.Value.Equals("0") && !String.IsNullOrEmpty(x.Value))
                             select new { MD5 = x.Value, id = x.FindAttribute("id").Value });

            foreach (var image in imagelist)
            {
                _md5File = image.MD5;
                await _QuokaHelper.UploadImage(string.Format("$(\"input:file\").last()"), image.MD5, null, "gif", _tcsImageUploaded.Task);
                await TaskEx.Delay(1000, _cancelToken);
            }
            return true;
        }
    }
}
