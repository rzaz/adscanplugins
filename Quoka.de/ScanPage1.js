﻿//////////////////////////////////////////////
//$SNIPPET: ScanPage1
//////////////////////////////////////////////

jq("a.button.blue.btn-msg-send span").html('Ascan Save');
jq("#UpsellHiddenChoices").remove();


var elements = [];

// catch rubriek icon click
jq(document.body).on('click', 'div.CategoryIcon div', function () {
    var element = {
        "Tagname": "catdiv",
        "Value": jq(this).attr('class')
    };
    elements.push(element);
});


jq("a.button.blue.btn-msg-send").on("click", function (ev) {
    console.log('submit');
    
    // catch subrubrieken via hidden input
    var test = jq("input:hidden[name='catid']").val();
    var hiddenelement = {
        "Tagname": jq(this).prop("rubriekselector"),
        "Parent": null,
        "Value": test
    };
    elements.push(hiddenelement);

/*
    //standaard zooi
    jq("#frmInsert select").each(function() {
        var element = {
            "Tagname": jq(this).prop("tagName"),
            "Parent": null,
            "Value": jq(this).val(),
            "Attributes": [
                {
                    "Name": "id",
                    "Value": jq(this).prop("id")
                }, {
                    "Name": "type",
                    "Value": jq(this).prop("type")
                }, {
                    "Name": "name",
                    "Value": jq(this).prop("name")
                }, {
                    "Name": "text",
                    "Value": jq(this).children("option:selected").text().trim()
                }
            ]
        };
        elements.push(element);
    });

    jq("#frmInsert input:text").not(":hidden").each(function() {
        var element = {
            "Tagname": jq(this).prop("tagName"),
            "Parent": null,
            "Value": jq(this).val(),
            "Attributes": [
                {
                    "Name": "id",
                    "Value": jq(this).prop("id")
                }, {
                    "Name": "type",
                    "Value": jq(this).prop("type")
                }, {
                    "Name": "name",
                    "Value": jq(this).prop("name")
                }
            ]
        };
        elements.push(element);
    });

    jq("#frmInsert input:radio:checked").not(":hidden").each(function() {
        var element = {
            "Tagname": jq(this).prop("tagName"),
            "Parent": null,
            "Value": 1,
            "Attributes": [
                {
                    "Name": "id",
                    "Value": jq(this).prop("id")
                }, {
                    "Name": "type",
                    "Value": jq(this).prop("type")
                }, {
                    "Name": "name",
                    "Value": jq(this).prop("name")
                }
            ]
        };
        elements.push(element);
    });

    jq("#frmInsert input:checkbox").not(":hidden").each(function () {
        var element = {
            "Tagname": jq(this).prop("tagName"),
            "Parent": null,
            "Value": jq(this).prop("checked"),
            "Attributes": [
                {
                    "Name": "id",
                    "Value": jq(this).prop("id")
                }, {
                    "Name": "type",
                    "Value": jq(this).prop("type")
                }, {
                    "Name": "name",
                    "Value": jq(this).prop("name")
                }
            ]
        };
        elements.push(element);
    });

    jq("#frmInsert textarea").not(":hidden").each(function () {
        var element = {
            "Tagname": jq(this).prop("tagName"),
            "Parent": null,
            "Value": jq(this).val(),
            "Attributes": [
                {
                    "Name": "id",
                    "Value": jq(this).prop("id")
                }, {
                    "Name": "type",
                    "Value": jq(this).prop("type")
                }, {
                    "Name": "name",
                    "Value": jq(this).prop("name")
                }
            ]
        };
        elements.push(element);
    });
 */
    adscan.ScanPage(JSON.stringify(elements));
   
    ev.preventDefault();
    ev.stopPropagation();
    console.log('stoppen met die zooi');
    return false;
});
