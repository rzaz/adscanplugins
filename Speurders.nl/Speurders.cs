﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Core;
using Core.NavigationAsync;
using NLog;
using PluginFrameWork;
using PluginFrameWork.Ads;
using Attribute = PluginFrameWork.Ads.Attribute;

namespace Speurders
{
    public class Plugin : IPluginAsync, IPluginTemplateAsync
    {
        private Navigation _helper;
        private Helpers _speurdersHelper;
        private CancellationToken _cancelToken;
        private Ad _ad;
        private readonly Logger _log = LogManager.GetCurrentClassLogger();
        private string md5File = "";


        #region "IPluginAsync"
        Dictionary<string, string> images = new Dictionary<string, string>();

        public string Name { get { return "Speurders.nl"; } }
        public CancellationToken CancelToken { set { _cancelToken = value; } }

        public void Init(object webview)
        {
            _helper = new Navigation(webview, _cancelToken);
            _speurdersHelper = new Helpers(webview, _cancelToken);
            //_helper.NoConflictModeJquery = false;
            //_helper.AutoInjectJquery();
            _helper.AutoInjectScripts();

        }
        public async Task<bool> LoginAsync(Account account)
        {
            _helper.Navigate("http://www.speurders.nl/logout");
            await _helper.WaitForFinishAsync();
            _helper.Navigate("http://www.speurders.nl/my_speurders.php");
            await _helper.WaitForFinishAsync();
            //page_login
            var result = await _helper.InvokeScriptStringAsync("$(\"h1:contains('Inloggen')\").not(\"h1:contains('Hyves')\").text()", true);
            if (!result.Equals("Inloggen"))
            {
                _log.Trace("Login not necessary");
                return true;
            }
            await _speurdersHelper.WaitUntilExistsAsync(Helpers.CreateSelector("email"));

            await _speurdersHelper.SetTextBoxAsync(Helpers.CreateSelector("email"), account.Name);
            await _speurdersHelper.SetTextBoxAsync(Helpers.CreateSelector("password"), account.Password);
            await _speurdersHelper.ClickAsync("$(\"input:submit:eq(0)\")");
            await TaskEx.Delay(1000, _cancelToken);
            if (!_speurdersHelper.InvokeScriptString("$(\".error\").text()").Equals(""))
            {
                throw new Exception(_speurdersHelper.InvokeScriptString("$(\".error\").text()"));
            }
            //await _helper.WaitForFinishAsync();
            return true;

        }

        public async Task<bool> FillAsync(Account account, Ad ad, bool catchExceptions)
        {
            images.Clear();
            _log.Debug("Wait for {0}", "http://www.speurders.nl/plaatsen");
            _helper.Navigate("http://www.speurders.nl/plaatsen");
            await _helper.WaitForFinishAsync("plaatsen");

            //await TaskEx.Delay(1000);
            _log.Debug("Done");

            //select boxes ==================================================================
            var selects = from Element x in ad.FindPage("page1").Elements where x.Tagname.Equals("SELECT") select x;

            _log.Debug("selects");
            foreach (var selectbox in selects)
            {

                var controlId = selectbox.FindAttribute("id").Value;
                _log.Trace(controlId);

                string[] selectors = { "contact-country_id", "contact-province" ,"duration"};
                if (!string.IsNullOrEmpty(selectbox.Value) && !selectors.Contains(controlId))
                {
                    _log.Trace("SetSelectboxAsync");
                    await _speurdersHelper.SetSelectboxAsync(Helpers.CreateSelector(controlId), selectbox.Value).TimeoutAfter(10000);
                }
            }

            _cancelToken.ThrowIfCancellationRequested();
            _log.Debug("radios");
            //vul alle radio buttons in
            foreach (var radioButton in Helpers.GetRadiosFromElements(ad.FindPage("page1").Elements))
            {
                await _speurdersHelper.ClickRadioAsync(Helpers.CreateSelector("INPUT", "name", radioButton.FindAttribute("name").Value));
            }

            _cancelToken.ThrowIfCancellationRequested();
            _log.Debug("checkboxen");
            //vul alle checkboxes in
            foreach (var checkBox in Helpers.GetCheckboxesFromElements(ad.FindPage("page1").Elements))
            {
                await _speurdersHelper.ClickCheckboxAsync(Helpers.CreateSelector(checkBox.FindAttribute("id").Value)).TimeoutAfter(1000);
            }

            _cancelToken.ThrowIfCancellationRequested();
            _log.Debug("textboxen");
            //vul alle checkboxes in
            foreach (var textBox in Helpers.GetTextBoxesFromElements(ad.FindPage("page1").Elements))
            {
                await _speurdersHelper.SetTextBoxAsync(Helpers.CreateSelector(textBox.FindAttribute("id").Value), textBox.Value.Replace("\\", "\\\\").Replace("'", "\\'")).TimeoutAfter(1000);
            }

            var tinymce = (from x in ad.FindPage("page1").Elements where x.Tagname.Equals("tinymce") select x).Single();

            var htmlConverter = new HtmlConverter();
            var keeplist = new[] { "UL", "LI" }.ToList();
            var replacelist = new List<Translate> { new Translate("P", "P", false) };


            {
                htmlConverter.TagsToKeep = keeplist;
                htmlConverter.ReplaceList = replacelist;
                htmlConverter.HmtlString = tinymce.Value;
                htmlConverter.convert();

            }
            var html = htmlConverter.ConvertedHtml;
            html = html.Replace("</BR>", "<BR>");
            html = html.Replace("<BR/>", "<BR>");
            html = html.Replace("\r\n", "<BR>'");
            html = html.Replace("\r", "<BR>'");
            html = html.Replace("\n", "<BR>'");
            html = html.Replace("'", "\\'");

            while (_helper.InvokeScriptString("tinyMCE.activeEditor.getContent()").Equals(""))
            {
                await TaskEx.Delay(500);
                _cancelToken.ThrowIfCancellationRequested();
                var text = string.Format("tinyMCE.activeEditor.setContent('{0}')", html);
                _helper.InvokeScriptString(text);
                //html = Replace(html, Chr(13), "") 
            }

            await UploadImagesInAd(ad).TimeoutAfter(60000);


            return true;

        }

        private async Task<Boolean> UploadImagesInAd(Ad ad)
        {
            ImageHook();
            var images = (from x in ad.FindPage("page1").Elements
                          where (x.Tagname.Equals("image", StringComparison.InvariantCultureIgnoreCase)
                          && !x.Value.Equals("0") && !String.IsNullOrEmpty(x.Value))
                          select new { MD5 = x.Value, id = x.FindAttribute("id").Value });
            int num = 0;
            foreach (var image in images)
            {
                _tcsImageUploaded = new TaskCompletionSource<bool>();
                md5File = image.MD5;
                await _speurdersHelper.UploadImage(string.Format("$(\"#photo-uploader-{0}\").eq(0)", 1), image.MD5, null, "gif", _tcsImageUploaded.Task);
                num++;
                await TaskEx.Delay(1000,_cancelToken);
            }
            return true;
        }

        public async Task<bool> ConfirmAdAsync(Ad ad, bool manualClick)
        {
       
            //await _speurdersHelper.ClickAsync("$(\"#cookie_consent_accept\")");
            //await TaskEx.Delay(5000);
            _log.Info("ConfirmAdAsyncc");
            await _speurdersHelper.ClickAsync("$(\"#next_step\")");
            await _helper.WaitForFinishAsync();
            await _speurdersHelper.ClickAsync("$(\"#place_now\")");
            await _helper.WaitForFinishAsync();
            

            var id = await _helper.InvokeScriptStringAsync("$(\"a.btn-cta:contains('Bekijk')\").prop(\"href\");", true);
            ad.LastPlaced = DateTime.Now;
            ad.LastPlacedURL = id; //id.Substring(id.ToString().LastIndexOf("-", StringComparison.Ordinal) + 1).Replace(".html", "");

            return true;
        }

        public async Task<bool> RecordAsync(Account account, Ad ad)
        {
            images.Clear();
            _helper.Navigate("http://www.speurders.nl/plaatsen");
            await _helper.WaitForFinishAsync("plaatsen");

            _speurdersHelper.TrackManualImageUploadsBegin(md5 => { md5File = md5; }, "gif");
            await _helper.InvokeScriptStringAsync("$(\"#place_now\").val('Sla op in Adscan');", true);
            await _helper.InvokeScriptStringAsync("$(\"#category_id\").on(\"change\",\"\",function() {setTimeout(function() { $(\"#place_now\").val('Sla op in Adscan');},1000);});", true);
            var listenPage = PageHookAsync();
            var elements = await listenPage;
            ad.Pages.Add(new Page());
            ad.Pages.Last().Name = "page1";
            ad.Pages.Last().Url = _helper.InvokeScriptString("document.URL");
            ad.Pages.Last().Elements.AddRange(elements);
            ad.Account = account;

            _speurdersHelper.TrackManualImageUploadsEnd();
            _speurdersHelper.CleanTemporaryImages();


            return true;
        }

        private async Task<DeleteResult> DeleteEasy(String title, String id)
        {
            Func<Task<Boolean>> delete = async () =>
            {
                if (_helper.InvokeScriptString(String.Format("$(\".action-check-item[data-ad-id={0}]\").length", id)) !=
                    "0")
                {
                    _log.Debug("found id");
                    _helper.InvokeScriptString(String.Format("$(\".action-check-item[data-ad-id={0}]\").click()", id));
                    _helper.InvokeScriptString("$(\"button.btn.action-delete\").click()");
                    await _helper.WaitForFinishAsync();
                    return true;
                }
                return false;
            };

            await _speurdersHelper.WaitUntilExistsAsync("$(\"input:text[name='query']\")");
            if (await delete())
            {
                return DeleteResult.Deleted;
            }

            //niet op op pagina 1 gevonden dus eerst nog een keer de titel invullen

            _helper.InvokeScriptString(String.Format("$(\"input:text[name='query']\").val('{0}')", title));
            _helper.InvokeScriptString("$(\"form.form-search input.btn:submit\").click()");
            await _helper.WaitForFinishAsync();
            if (await delete())
            {
                return DeleteResult.Deleted;
            }


            return DeleteResult.NotFound;
        }

        public async Task<DeleteResult> DeleteAsync(Account account, Ad ad, bool guessMode)
        {
            _helper.Navigate("http://www.speurders.nl/lopend/");
            await _helper.WaitForFinishAsync("lopend");
            var id = "";
            try
            {
                id =
                    ad.LastPlacedURL.Substring(ad.LastPlacedURL.LastIndexOf("-", StringComparison.Ordinal) + 1)
                        .Replace(".html", "");
            }
            catch (Exception){}

            if (guessMode)
            {
                //guessmode nog implementeren
            
            }

            return await DeleteEasy(System.Text.RegularExpressions.Regex.Replace(Title, "\\s+", " ")
                .Replace("'", "\\'").Trim(), id);
            
        }

        async public Task<bool> EditAsync(Account account, Ad ad, Action removeSplash)
        {
            _log.Info("edit mode");
            images.Clear();
            await FillAsync(account, ad,true);
            removeSplash();
            await _helper.InvokeScriptStringAsync("$(\"#place_now\").val('Update');", true);
            //$("#place_now").val('Poep');
            //_tcsImageUploaded = new TaskCompletionSource<bool>();

            _speurdersHelper.TrackManualImageUploadsBegin(md5 => { md5File = md5; }, "gif");

            var listenPage = PageHookAsync();

            var page1 = ad.FindPage("page1");
            ad.Pages.Remove(page1);

            var elements = await listenPage;
            ad.Pages.Add(new Page());
            ad.Pages.Last().Name = "page1";
            ad.Pages.Last().Url = _helper.InvokeScriptString("document.URL");
            ad.Pages.Last().Elements.AddRange(elements);

            _speurdersHelper.TrackManualImageUploadsEnd();
            _speurdersHelper.CleanTemporaryImages();

            return true;
        }

        public PluginStatus Status { get; set; }
        #endregion

        #region "IPluginTemplateAsync"

        public void SetAd(Ad ad)
        {
            _ad = ad;
        }

        public string Title
        {
            get { return FindElement("INPUT", "id", "title").Value; }
            set { FindElement("INPUT", "id", "title").Value = value.TakeFirst(60); }
        }

        public string Description
        {
            get { return FindElement("tinymce", "", "").Value; }
            set { FindElement("tinymce", "", "").Value = value; }
        }
        public string Price
        {
            get { return FindElement("INPUT", "id", "price").Value; }
            set { FindElement("INPUT", "id", "price").Value = value; }
        }
        public string GetImage(int number)
        {
            return FindElement("image", "id", number.ToString()).Value;
        }

        public void SetImage(int number, string value)
        {
            FindElement("image", "id", number.ToString()).Value = value;
        }

        public string Extra { get; set; }

        public string BareAdName { get { return Title; } }

        private Element FindElement(string tagName, string attributeName, string lookForAttribute)
        {
            Element tempElement;
            if (string.IsNullOrEmpty(attributeName))
            {
                tempElement = (from x in _ad.FindPage("page1").Elements
                               where !string.IsNullOrEmpty(x.Tagname) && x.Tagname.Equals(tagName)
                               select x).SingleOrDefault();

            }
            else
            {
                List<Element> allElementsWithAttributeName = (from x in _ad.FindPage("page1").Elements where x.FindAttribute(attributeName) != null && x.Tagname.Equals(tagName) select x).ToList();
                tempElement = (from x in allElementsWithAttributeName where x.FindAttribute(attributeName).Value.Equals(lookForAttribute) select x).SingleOrDefault();
            }

            if (tempElement != null)
            {
                return tempElement;
            }
            var el = new Element { Tagname = tagName };
            el.Attributes.Add(new Attribute());
            el.Attributes.Last().Name = attributeName;
            el.Attributes.Last().Value = lookForAttribute;
            el.Attributes.Add(new Attribute());
            el.Attributes.Last().Name = "type";
            el.Attributes.Last().Value = "file";
            _ad.Pages.Last().Elements.Add(el);
            return el;
        }
        #endregion

        #region "Hooks"

        private TaskCompletionSource<Boolean> _tcsImageUploaded;

        private void ImageHook()
        {
            const string script = @"
var lastClickedImageId = 0;
var uploading = false;

$(""html"").on(""mousedown"", ""[id^='photo-uploader']"", function(ev) {
	if (uploading) {
		ev.preventDefault();
		ev.stopPropagation();
		return false;
	};
	uploading = true;
	lastClickedImageId += 1;
	console.log('image ' + lastClickedImageId + ' clicked');
	//adscan.imageClick(lastClickedImageId);
})

$(""#sortable-photos"").on(""mouseup"", ""span.icon-trashcan"", function() {
	//console.log('delete ' + lastClickedImageId + ' clicked');
	lastClickedImageId -= 1;
	var clickedImage = ($("".uploaded-photo"").index($(this).parent().parent().parent().parent()) + 1);
	console.log('delete ' + clickedImage + ' clicked');
	adscan.imageDelete(clickedImage);
})


$(""input"").change(function() {
	console.log('waiting for ' + lastClickedImageId);
	var completed = $(this).find("".thumbnail-pic"").length;
	window.checker = setInterval(function() {
		if ($(""html"").find("".thumbnail-pic"").length != completed) {
			clearInterval(window.checker);
			console.log('uploaded image with id ' + lastClickedImageId);
            if (uploading) {
			    adscan.imageUploaded(lastClickedImageId);
            }
			uploading = false;
		};
	}, 200);
});
";
            _helper.InvokeScriptString(script);





            _helper.CreateJavascriptCallback("imageDelete", (methodName, args) =>
            {
                _log.Info("Deleting {0}", args[0].ToString());
                images.Remove(args[0].ToString());
                return "";
            });
            _helper.CreateJavascriptCallback("imageUploaded", (methodName, args) =>
            {
                if (_tcsImageUploaded != null) _tcsImageUploaded.TrySetResult(true);
                _log.Info("Adding {0}", args[0].ToString());
                images.Add(args[0].ToString(), md5File);
                return "";
            });



        }

        //private TaskCompletionSource<List<Element>> tcsPageHook;
        private async Task<List<Element>> PageHookAsync()
        {

            var tcsPageHook = new TaskCompletionSource<List<Element>>();
            _cancelToken.Register(tcsPageHook.SetCanceled);
            _helper.CreateJavascriptCallback("ScanPage", (method, args) =>
            {
                _log.Debug("Entering scanroutine");
                var test = _speurdersHelper.JSonToObject<List<Element>>(args[0].ToString());
                tcsPageHook.SetResult(test);
                return "";
            });

            const string script = @"
$(""#logo"").hide();
$(""#tr_OMH"").hide();
$(""#omh_window"").hide();
$(""#place_form"").attr('action', 'no');
$(""html"").on(""submit"", ""#ad-form"", function (ev) {
	var elements = [];
	$(""input:text"").not("":hidden"").each(function () {
		var element = {
			""Tagname"" : $(this).prop(""tagName""),
			""Parent"" : null,
			""Value"" : $(this).val(),
			""Attributes"" : [{
					""Name"" : ""id"",
					""Value"" : $(this).prop(""id"")
				}, {
					""Name"" : ""type"",
					""Value"" : $(this).prop(""type"")
				}, {
					""Name"" : ""name"",
					""Value"" : $(this).prop(""name"")
				}
			]
		}
		elements.push(element);
	});
	$(""input:checkbox:checked"").not("":hidden"").each(function () {
		var element = {
			""Tagname"" : $(this).prop(""tagName""),
			""Parent"" : null,
			""Value"" : 1,
			""Attributes"" : [{
					""Name"" : ""id"",
					""Value"" : $(this).prop(""id"")
				}, {
					""Name"" : ""type"",
					""Value"" : $(this).prop(""type"")
				}, {
					""Name"" : ""name"",
					""Value"" : $(this).prop(""name"")
				}
			]
		}
		elements.push(element);
	});
	$(""input:radio:checked"").each(function () {
		var element = {
			""Tagname"" : $(this).prop(""tagName""),
			""Parent"" : null,
			""Value"" : 1,
			""Attributes"" : [{
					""Name"" : ""id"",
					""Value"" : $(this).prop(""id"")
				}, {
					""Name"" : ""type"",
					""Value"" : $(this).prop(""type"")
				}, {
					""Name"" : ""name"",
					""Value"" : $(this).prop(""name"")
				}
			]
		}
		elements.push(element);
	});
	$(""select:not([disabled])"").each(function () {
		var element = {
			""Tagname"" : $(this).prop(""tagName""),
			""Parent"" : null,
			""Value"" : $(this).val(),
			""Attributes"" : [{
					""Name"" : ""id"",
					""Value"" : $(this).prop(""id"")
				}, {
					""Name"" : ""type"",
					""Value"" : $(this).prop(""type"")
				}, {
					""Name"" : ""name"",
					""Value"" : $(this).prop(""name"")
				}, {
					""Name"" : ""text"",
					""Value"" : $(this).children(""option:selected"").text()
				}
			]
		}
		elements.push(element);
	});
	var element = {
		""Tagname"" : ""tinymce"",
		""Parent"" : null,
		""Value"" : tinyMCE.activeEditor.getContent(),
	}
	elements.push(element);

	adscan.ScanPage(JSON.stringify(elements));

	ev.preventDefault();
	ev.stopPropagation();
	return false;
});


";

            _helper.InvokeScriptString(script);
            var allElements = await tcsPageHook.Task;
            foreach (var image in images)
            {
                var element = new Element { Tagname = "image" };
                element.Attributes.Add(new Attribute { Name = "id", Value = image.Key });
                element.Attributes.Add(new Attribute { Name = "type", Value = "file" });
                element.Attributes.Add(new Attribute { Name = "name", Value = image.Value });
                element.Value = image.Value;
                allElements.Add(element);
            }

            return allElements;

        }
        #endregion

    }
}
