﻿//////////////////////////////////////////////
//$SNIPPET: ScanPage1
//////////////////////////////////////////////

$("#ctl00_MainContent_ContentPlaceHolder_btnSaveAd").val('Ascan Save');



$("#aspnetForm").on("submit", function (ev) {
    var elements = [];
    $("#aspnetForm select").each(function () {
        var element = {
            "Tagname": $(this).prop("tagName"),
            "Parent": null,
            "Value": $(this).val(),
            "Attributes": [
                {
                    "Name": "id",
                    "Value": $(this).prop("id")
                }, {
                    "Name": "type",
                    "Value": $(this).prop("type")
                }, {
                    "Name": "name",
                    "Value": $(this).prop("name")
                }, {
                    "Name": "text",
                    "Value": $(this).children("option:selected").text().trim()
                }
            ]
        }
        elements.push(element);
    });

    $("#aspnetForm input:text").not(":hidden").each(function () {
        var element = {
            "Tagname": $(this).prop("tagName"),
            "Parent": null,
            "Value": $(this).val(),
            "Attributes": [
                {
                    "Name": "id",
                    "Value": $(this).prop("id")
                }, {
                    "Name": "type",
                    "Value": $(this).prop("type")
                }, {
                    "Name": "name",
                    "Value": $(this).prop("name")
                }
            ]
        }
        elements.push(element);
    });


    adscan.ScanPage(JSON.stringify(elements));
    ev.preventDefault();
    ev.stopPropagation();
    return false;
});



