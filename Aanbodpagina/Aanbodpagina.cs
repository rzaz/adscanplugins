﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Aanbodpagina.Properties;
using Core;
using Core.My;
using Core.NavigationAsync;
using NLog;
using NLog.Targets;
using PluginFrameWork;
using PluginFrameWork.Ads;
using Attribute = PluginFrameWork.Ads.Attribute;

namespace Aanbodpagina
{
    public class Plugin : IPluginAsync, IPluginTemplateAsync
    {
        private Navigation _helper;
        private Helpers _aanbodpaginaHelper;
        private CancellationToken _cancelToken;
        private Ad _ad;
        private readonly Logger _log = LogManager.GetCurrentClassLogger();
        private string _md5File = "";

        #region "IPluginAsync"

        Dictionary<string, string> images = new Dictionary<string, string>();
        private TaskCompletionSource<bool> _tcsImageUploaded;

        public CancellationToken CancelToken { set { _cancelToken = value; } }

        public string Name { get { return "Aanbodpagina"; } }

        public void Init(object webview)
        {
            _helper = new Navigation(webview, _cancelToken);
            _aanbodpaginaHelper = new Helpers(webview, _cancelToken);
            _helper.AutoInjectJquery();
            _helper.AutoInjectScripts();
        }

        public async Task<bool> LoginAsync(Account account)
        {
            _helper.Navigate("	http://advertenties.aanbodpagina.nl/logout.aspx");
            await _helper.WaitForFinishAsync();
            _helper.Navigate("http://advertenties.aanbodpagina.nl/Login.aspx");
            await _helper.WaitForFinishAsync();

            //Cleanpage();
            await _aanbodpaginaHelper.WaitUntilExistsAsync(Helpers.CreateSelector("ctl00_MainContent_ContentPlaceHolder_EmailTextBox"));
            await _aanbodpaginaHelper.SetTextBoxAsync(Helpers.CreateSelector("ctl00_MainContent_ContentPlaceHolder_EmailTextBox"), account.Name);
            await _aanbodpaginaHelper.SetTextBoxAsync(Helpers.CreateSelector("ctl00_MainContent_ContentPlaceHolder_PasswordTextBox"), account.Password);
            await _aanbodpaginaHelper.ClickAsync("$(\"input:submit[value='Login']\")");
            await _helper.WaitForFinishAsync(); //wachten tot we naar "uw advertenties" worden herleid
            return true;
        }

        //******************    so far ready

        public async Task<bool> RecordAsync(Account account, Ad ad)
        {
            _helper.Navigate("http://advertenties.aanbodpagina.nl");
            await _helper.WaitForFinishAsync("aanbodpagina.nl");
            await TaskEx.Delay(200, _cancelToken);

            images.Clear();

            var elements = await _helper.EnsureTaskWithScriptsAsync(ScanPage1(),
                                                         () => _helper.InvokeScriptString(_helper.GetJavascript(Resources.ScanPage1, "ScanPage1")));


            ad.Pages.Clear();
            ad.Pages.Add(new Page());
            ad.Pages.Last().Name = "page1";
            ad.Pages.Last().Url = _helper.InvokeScriptString("document.URL");
            ad.Pages.Last().Elements.AddRange(elements);
            ad.Account = account;
            return true;
        }

        public Task<bool> EditAsync(Account account, Ad ad, Action removeSplash)
        {
            throw new NotImplementedException();
        }

        public Task<bool> FillAsync(Account account, Ad ad, bool catchExceptions)
        {
            throw new NotImplementedException();
        }

        public Task<bool> ConfirmAdAsync(Ad ad, bool manualClick)
        {
            throw new NotImplementedException();
        }

        public Task<DeleteResult> DeleteAsync(Account account, Ad ad, bool guessMode)
        {
            throw new NotImplementedException();
        }

        public PluginStatus Status { get; set; }

        #endregion

        #region "IPluginTemplateAsync"

        public void SetAd(Ad ad)
        {
            _ad = ad;
        }

        public string Title
        {
            get { return FindElement("INPUT", "id", "ctl00_MainContent_ContentPlaceHolder_AdTitleTextBox").Value; }
            set { FindElement("INPUT", "id", "ctl00_MainContent_ContentPlaceHolder_AdTitleTextBox").Value = value; }
        }

        public string Price
        {
            get { return FindElement("INPUT", "id", "ctl00_MainContent_ContentPlaceHolder_AdPriceTextBox").Value; }
            set { FindElement("INPUT", "id", "ctl00_MainContent_ContentPlaceHolder_AdPriceTextBox").Value = value; }
        }

        public string Description
        {
            get { return FindElement("textarea", "id", "ctl00_MainContent_ContentPlaceHolder_AdTextTextBox").Value; }
            set { FindElement("textarea", "id", "ctl00_MainContent_ContentPlaceHolder_AdTextTextBox").Value = value; }
        }

        public string GetImage(int number)
        {
            return FindElement("image", "id", number.ToString()).Value;
        }

        public void SetImage(int number, string value)
        {
            FindElement("image", "id", number.ToString()).Value = value;
        }

        public string Extra { get; set; }

        public string BareAdName{get { return Title; }}

        private Element FindElement(string tagName, string attributeName, string lookForAttribute)
        {
            Element tempElement;
            if (string.IsNullOrEmpty(attributeName))
            {
                tempElement = (from x in _ad.FindPage("page1").Elements
                               where !string.IsNullOrEmpty(x.Tagname) && x.Tagname.Equals(tagName)
                               select x).SingleOrDefault();

            }
            else
            {
                List<Element> allElementsWithAttributeName = (from x in _ad.FindPage("page2").Elements where x.FindAttribute(attributeName) != null select x).ToList();
                tempElement = (from x in allElementsWithAttributeName where x.FindAttribute(attributeName).Value.Equals(lookForAttribute) select x).SingleOrDefault();
            }

            if (tempElement != null)
            {
                return tempElement;
            }
            var el = new Element { Tagname = tagName };
            el.Attributes.Add(new Attribute());
            el.Attributes.Last().Name = attributeName;
            el.Attributes.Last().Value = lookForAttribute;
            el.Attributes.Add(new Attribute());
            el.Attributes.Last().Name = "type";
            el.Attributes.Last().Value = "file";
            _ad.Pages.Last().Elements.Add(el);
            return el;
        }

        #endregion


        private async Task<List<Element>> ScanPage1()
        {
            var tcsPageHook = new TaskCompletionSource<List<Element>>();
            _helper.CreateJavascriptCallback("ScanPage", (method, args) =>
            {
                _log.Debug("Entering scanroutine");
                var test = _aanbodpaginaHelper.JSonToObject<List<Element>>(args[0].ToString());
                tcsPageHook.TrySetResult(test);
                return "";
            });
            var allElements = await tcsPageHook.Task;
            return allElements;
        }

        private async Task<List<Element>> ScanPage2()
        {

            var tcsPageHook = new TaskCompletionSource<List<Element>>();
            _helper.CreateJavascriptCallback("ScanPage", (method, args) =>
            {
                _log.Debug("Entering scanroutine");
                var test = _aanbodpaginaHelper.JSonToObject<List<Element>>(args[0].ToString());
                tcsPageHook.SetResult(test);
                return "";
            });

            await _helper.InvokeScriptStringAsync(
                _helper.GetJavascript(Resources.ScanPage2, "ScanPage2"), true);

            var allElements = await tcsPageHook.Task;
            foreach (var image in images)
            {
                var element = new Element { Tagname = "image" };
                element.Attributes.Add(new Attribute { Name = "id", Value = image.Key });
                element.Attributes.Add(new Attribute { Name = "type", Value = "file" });
                element.Attributes.Add(new Attribute { Name = "name", Value = image.Value });
                element.Value = image.Value;
                allElements.Add(element);
            }

            return allElements;
        }

        private async Task<Boolean> UploadImagesInAd(Ad ad)
        {
            DetectImageChanges();

            var imagelist = (from x in ad.FindPage("page2").Elements
                             where (x.Tagname.Equals("image", StringComparison.InvariantCultureIgnoreCase)
                             && !x.Value.Equals("0") && !String.IsNullOrEmpty(x.Value))
                             select new { MD5 = x.Value, id = x.FindAttribute("id").Value });

            foreach (var image in imagelist)
            {
                _md5File = image.MD5;
                await _aanbodpaginaHelper.UploadImage(string.Format("$(\"input:file\").last()"), image.MD5, null, "gif", _tcsImageUploaded.Task);
                await TaskEx.Delay(1000, _cancelToken);
            }
            return true;
        }

        private void DetectImageChanges()
        {
            _tcsImageUploaded = new TaskCompletionSource<bool>();

            _helper.CreateJavascriptCallback("imageUploaded", (method, args) =>
            {
                _log.Debug("Entering scanroutine");
                images.Add(args[0].ToString(), _md5File);
                _tcsImageUploaded.TrySetResult(true);
                _tcsImageUploaded = new TaskCompletionSource<bool>();
                return "";
            });

            _helper.CreateJavascriptCallback("imageDelete", (method, args) =>
            {
                _log.Debug("Entering  image delete routine");
                _log.Debug(args[0].ToString());
                images.Remove(args[0].ToString());
                return "";
            });

            _helper.InvokeScriptStringAsync(
                _helper.GetJavascript(Resources.ImageScripts, "UploadFinished"), true);
        }




    }






}
