﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core;
using Core.NavigationAsync;
using NLog;
using PluginFrameWork;
using PluginFrameWork.Ads;
using Attribute = PluginFrameWork.Ads.Attribute;

namespace Marktnet
{
    public class Plugin : IPluginAsync, IPluginTemplateAsync
    {
        private Navigation _helper;
        private Helpers _marktnetHelper;
        private CancellationToken _cancelToken;
        private Ad _ad;
        private readonly Logger _log = LogManager.GetCurrentClassLogger();
        private string md5File = "";

        #region "IPluginAsync"
        Dictionary<string, string> images = new Dictionary<string, string>();

        public string Name { get { return "Marktnet.nl"; } }
        public CancellationToken CancelToken { set { _cancelToken = value; } }

        public void Init(object webview)
        {
            _helper = new Navigation(webview, _cancelToken);
            _marktnetHelper = new Helpers(webview, _cancelToken);
            //_helper.NoConflictModeJquery = false;
            //_helper.AutoInjectJquery();_helper.AutoInjectScripts();

        }

        public async Task<bool> LoginAsync(Account account)
        {
            await TaskEx.Delay(1, _cancelToken);
            return true;

        }

        public async Task<bool> FillAsync(Account account, Ad ad, bool catchExceptions)
        {
            await TaskEx.Delay(1, _cancelToken);
            return true;

        }


        public async Task<bool> ConfirmAdAsync(Ad ad, bool manualClick)
        {
            await TaskEx.Delay(1, _cancelToken);
            return true;
        }

        public async Task<bool> RecordAsync(Account account, Ad ad)
        {
            await TaskEx.Delay(1, _cancelToken);
            return true;
        }



        public async Task<DeleteResult> DeleteAsync(Account account, Ad ad, bool guessMode)
        {
            await TaskEx.Delay(1, _cancelToken);
            return DeleteResult.Deleted;
        }

        public async Task<bool> EditAsync(Account account, Ad ad, Action removeSplash)
        {
            await TaskEx.Delay(1, _cancelToken);
            return true;
        }

        public PluginStatus Status { get; set; }
        #endregion

        #region "IPluginTemplateAsync"

        public void SetAd(Ad ad)
        {
            _ad = ad;
        }

        public string Title
        {
            get { return FindElement("INPUT", "id", "title").Value; }
            set { FindElement("INPUT", "id", "title").Value = value; }
        }

        public string Description
        {
            get { return FindElement("tinymce", "", "").Value; }
            set { FindElement("tinymce", "", "").Value = value; }
        }
        public string Price
        {
            get { return FindElement("INPUT", "id", "price").Value; }
            set { FindElement("INPUT", "id", "price").Value = value; }
        }
        public string GetImage(int number)
        {
            return FindElement("image", "id", number.ToString()).Value;
        }

        public void SetImage(int number, string value)
        {
            FindElement("image", "id", number.ToString()).Value = value;
        }

        public string Extra { get; set; }

        public string BareAdName { get { return Title; } }

        private Element FindElement(string tagName, string attributeName, string lookForAttribute)
        {
            Element tempElement;
            if (string.IsNullOrEmpty(attributeName))
            {
                tempElement = (from x in _ad.FindPage("page1").Elements
                               where !string.IsNullOrEmpty(x.Tagname) && x.Tagname.Equals(tagName)
                               select x).SingleOrDefault();

            }
            else
            {
                List<Element> allElementsWithAttributeName = (from x in _ad.FindPage("page1").Elements where x.FindAttribute(attributeName) != null select x).ToList();
                tempElement = (from x in allElementsWithAttributeName where x.FindAttribute(attributeName).Value.Equals(lookForAttribute) select x).SingleOrDefault();
            }

            if (tempElement != null)
            {
                return tempElement;
            }
            var el = new Element { Tagname = tagName };
            el.Attributes.Add(new Attribute());
            el.Attributes.Last().Name = attributeName;
            el.Attributes.Last().Value = lookForAttribute;
            el.Attributes.Add(new Attribute());
            el.Attributes.Last().Name = "type";
            el.Attributes.Last().Value = "file";
            _ad.Pages.Last().Elements.Add(el);
            return el;
        }
        #endregion

    }
}
