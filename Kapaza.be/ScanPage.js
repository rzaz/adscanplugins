﻿//////////////////////////////////////////////
//$SNIPPET: ScanPage
//////////////////////////////////////////////

$("#ai_form input:submit").attr("value", "Ascan Save");

$("#loading").remove();

$("#ai_form").submit(function (ev) {
    var elements = [];
    $("#ai_form select").each(function () {
        var element = {
            "Tagname": $(this).prop("tagName"),
            "Parent": null,
            "Value": $(this).val(),
            "Attributes": [{
                "Name": "id",
                "Value": $(this).prop("id")
            }, {
                "Name": "type",
                "Value": $(this).prop("type")
            }, {
                "Name": "name",
                "Value": $(this).prop("name")
            }, {
                "Name": "text",
                "Value": $(this).children("option:selected").text().trim()
            }]
        }
        elements.push(element);
    })

    $("#ai_form input[type=number],input:text").not(":hidden").each(function () {
        var element = {
            "Tagname": $(this).prop("tagName"),
            "Parent": null,
            "Value": $(this).val(),
            "Attributes": [{
                "Name": "id",
                "Value": $(this).prop("id")
            }, {
                "Name": "type",
                "Value": $(this).prop("type")
            }, {
                "Name": "name",
                "Value": $(this).prop("name")
            }]
        }
        elements.push(element);
    })

    $("#ai_form input:radio:checked").not(":hidden").each(function () {
        var element = {
            "Tagname": $(this).prop("tagName"),
            "Parent": null,
            "Value": 1,
            "Attributes": [{
                "Name": "id",
                "Value": $(this).prop("id")
            }, {
                "Name": "type",
                "Value": $(this).prop("type")
            }, {
                "Name": "name",
                "Value": $(this).prop("name")
            }]
        }
        elements.push(element);
    })

    $("#ai_form input:checkbox").not(":hidden").each(function () {
        var element = {
            "Tagname": $(this).prop("tagName"),
            "Parent": null,
            "Value": $(this).prop("checked"),
            "Attributes": [{
                "Name": "id",
                "Value": $(this).prop("id")
            }, {
                "Name": "type",
                "Value": $(this).prop("type")
            }, {
                "Name": "name",
                "Value": $(this).prop("name")
            }]
        }
        elements.push(element);
    });

    var element = {
        "Tagname": $("#ai_form textarea").prop("tagName"),
        "Parent": null,
        "Value": $("#ai_form textarea").val(),
        "Attributes": [{
            "Name": "id",
            "Value": $("#ai_form textarea").prop("id")
        }]
    }
    elements.push(element);
    /*
                        $("#ai_form textarea").not(":hidden").each(function() {
                            adscan.scan($(this));
                        })
    
                        $("input:file").not(":hidden").each(function() {
                            adscan.scan($(this));
                        })
    */
    adscan.ScanPage(JSON.stringify(elements));
    ev.preventDefault();
    ev.stopPropagation();
    return false;
});