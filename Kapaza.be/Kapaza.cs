﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Core;
using Core.My;
using Core.NavigationAsync;
using Kapaza.Properties;
using NLog;
using NLog.Targets;
using NLog.Targets.Wrappers;
using PluginFrameWork;
using PluginFrameWork.Ads;
using Attribute = PluginFrameWork.Ads.Attribute;

namespace Kapaza
{
    public class Plugin : IPluginAsync, IPluginTemplateAsync
    {
        private Navigation _helper;
        private Core.NavigationAsync.Kapaza.Helpers _kapazaHelper;
        private CancellationToken _cancelToken;
        private Ad _ad;
        private readonly Logger _log = LogManager.GetCurrentClassLogger();
        private string md5File = "";

        #region "IPluginAsync"
        Dictionary<string, string> images = new Dictionary<string, string>();
        private TaskCompletionSource<bool> _tcsImageUploaded;

        public string Name { get { return "Kapaza.be"; } }
        public CancellationToken CancelToken { set { _cancelToken = value; } }

        public void Init(object webview)
        {
            _helper = new Navigation(webview, _cancelToken);
            _kapazaHelper = new Core.NavigationAsync.Kapaza.Helpers(webview, _cancelToken);
            //_helper.NoConflictModeJquery = false;
            //_helper.AutoInjectJquery();
            _helper.AutoInjectScripts();

        }

        public async Task<bool> LoginAsync(Account account)
        {
            _helper.Navigate("http://www2.kapaza.be/nl/store/logout/0?url=http://www2.kapaza.be/nl/store/account/0");
            await _helper.WaitForFinishAsync();
            _helper.Navigate("http://www2.kapaza.be/store/login");
            await _helper.WaitForFinishAsync("login");
            await _kapazaHelper.WaitUntilExistsAsync(Helpers.CreateSelector("store_username"));
            await _kapazaHelper.SetTextBoxAsync(Helpers.CreateSelector("store_username"), account.Name);
            await _kapazaHelper.SetTextBoxAsync(Helpers.CreateSelector("input", "type", "password"), account.Password);
            await _kapazaHelper.ClickAsync(Helpers.CreateSelector("input", "type", "submit"));
            await _helper.WaitForFinishAsync(); //wachten tot we naar "uw advertenties" worden herleid
            return true;

        }

        public async Task<bool> FillAsync(Account account, Ad ad, bool catchExceptions)
        {
            _helper.Navigate("http://www2.kapaza.be/ai/form/");
            await _helper.WaitForFinishAsync("ai/form/").TimeoutAfter(15000);
            await TaskEx.Delay(200,_cancelToken);
            while (_helper.InvokeScriptString("$(\"button.button\").html()").Contains("undefined"))
            {
                await TaskEx.Delay(500, _cancelToken);
            }
            images.Clear();

            _helper.InvokeScriptString("$(\"button[name='validate']\").html('adscan save')");

            //_log.Debug("Wait for {0}", "http://www2.kapaza.be/nl/ai/form/0");
            //_helper.Navigate("http://www2.kapaza.be/nl/ai/form/0");
            //await _helper.WaitForFinishAsync();

            //await TaskEx.Delay(1000);
            _log.Debug("Done");

            //select boxes ==================================================================
            var selects = from Element x in ad.FindPage("page1").Elements where x.Tagname.Equals("SELECT") select x;

            _log.Debug("selects");
            foreach (var selectbox in selects)
            {

                var controlId = selectbox.FindAttribute("id").Value;
                _log.Trace(controlId);

                //string[] selectors = { "contact-country_id", "contact-province" };
                //if (!string.IsNullOrEmpty(selectbox.Value) && !selectors.Contains(controlId))
                //{
                _log.Trace("SetSelectboxAsync");
                await _kapazaHelper.SetSelectboxAsync(Helpers.CreateSelector(controlId), selectbox.Value).TimeoutAfter(5000);
                //}
            }

            _cancelToken.ThrowIfCancellationRequested();
            _log.Debug("radios");
            //vul alle radio buttons in
            foreach (var radioButton in Helpers.GetRadiosFromElements(ad.FindPage("page1").Elements))
            {
                await _kapazaHelper.ClickRadioAsync(Helpers.CreateSelector("INPUT", "name", radioButton.FindAttribute("name").Value)).TimeoutAfter(5000);
                _log.Debug("radio " + radioButton.FindAttribute("name").Value);
                await TaskEx.Delay(1);
            }

            _cancelToken.ThrowIfCancellationRequested();
            _log.Debug("checkboxen");
            //vul alle checkboxes in
            foreach (var checkBox in Helpers.GetCheckboxesFromElements(ad.FindPage("page1").Elements))
            {
                if (checkBox.Value != "True" && checkBox.Value != "False") checkBox.Value = "False";
                await _kapazaHelper.ClickCheckboxAsync(Helpers.CreateSelector(checkBox.FindAttribute("id").Value), Boolean.Parse(checkBox.Value)).TimeoutAfter(1000);
                _log.Debug("checkbox " + checkBox.FindAttribute("id").Value);
                await TaskEx.Delay(1);
            }

            _cancelToken.ThrowIfCancellationRequested();
            _log.Debug("textboxen");
            //vul alle checkboxes in
            foreach (var textBox in Helpers.GetTextBoxesFromElements(ad.FindPage("page1").Elements))
            {
                if (!String.IsNullOrEmpty(textBox.Value))
                {
                    await _kapazaHelper.SetTextBoxAsync(Helpers.CreateSelector(textBox.FindAttribute("id").Value), textBox.Value.EscapeTextBox()).TimeoutAfter(1000);
                    _log.Debug("textbox " + textBox.FindAttribute("id").Value);
                    await TaskEx.Delay(1);
                }
            }

            var area = (from x in ad.FindPage("page1").Elements where x.Tagname.Equals("TEXTAREA") select x).Single();


            var id = area.FindAttribute("id").Value;
            var areatext = area.Value;
            var htmlConverter = new HtmlConverter();
            var keeplist = new List<string> { "UL", "LI" };

            var replacelist = new List<Translate> { new Translate("P", "P", false), new Translate("BR", "BR", false) };

            {
                htmlConverter.TagsToKeep = keeplist;
                htmlConverter.ReplaceList = replacelist;
                htmlConverter.HmtlString = areatext;
                htmlConverter.IgnoreStyles = true;
                htmlConverter.convert();
            }

            string html = htmlConverter.ConvertedHtml;
            html = html.HtmlDecode();
            html = html.Replace("\\", "\\\\");
            html = html.Replace("\r", "");
            html = html.Replace("\n", "\\n");
            html = html.Replace("'", "\\'");
            //html = ReplaceSpecialchars(html);
            _kapazaHelper.InvokeScriptString(string.Format("$(\"#{0}\").val('{1}')", id, html.TakeFirst(4000)));


            await UploadImagesInAd(ad).TimeoutAfter(60000);
            return true;

        }
        
        public async Task<bool> ConfirmAdAsync(Ad ad, bool manualClick)
        {
            var retry = true;
            while (retry)
            {
                if (!manualClick)
                {
                    await _kapazaHelper.ClickAsync("$(\"button.button\")").TimeoutAfter(25000);
                }
                else
                {
                    await _helper.InvokeScriptStringAsync("$(\"button.button\").val('Plaats')", true).TimeoutAfter(5000);
                    await _kapazaHelper.ScrollIntoFocusAsync("$(\"button.button\")").TimeoutAfter(15000);
                    _helper.InvokeScriptString(
                            String.Format("$(\"button.button\").fadeTo({0}, 0.5).fadeTo({1}, 1.0).fadeTo({0}, 0.5).fadeTo({1}, 1.0).fadeTo({0}, 0.5).fadeTo({1}, 1.0).fadeTo({0}, 0.5).fadeTo({1}, 1.0).fadeTo({0}, 0.5).fadeTo({1}, 1.0)", 1500, 500));

                }
                await _helper.WaitForFinishAsync().TimeoutAfter(25000);

                await TaskEx.Delay(5000, _cancelToken);

                if (_helper.InvokeScriptString("$(\"span.error\").length") != "0")
                    throw new Exception("Error");


                _helper.InvokeScriptString("$(\"input.button\").click();");
                await _helper.WaitForFinishAsync().TimeoutAfter(25000);


                if (_helper.InvokeScriptString("$('#category_group').length").Equals("1"))
                {
                    retry = true;
                }
                else
                {
                    retry = false;
                }
            }

            //("div.alert-success").length
            var test = _helper.InvokeScriptString("$(\"div.kop.groot.rood h1\").html()".Trim());

            _log.Trace(test.Equals("Advertentie geplaatst"));
            await TaskEx.Delay(5000, _cancelToken);

            _ad.LastPlaced = DateTime.Now;
            //Replace(id.Substring(id.ToString.LastIndexOf("-", StringComparison.Ordinal) + 1), ".html", "")
            _ad.LastPlacedURL = Title;

            //todo: LastPlaced & LastPlacedURL worden heel vaak niet opgeslagen wel in grid zichtbaar maar bij volgende run weer leeg?
            return true;
        }

        public async Task<bool> RecordAsync(Account account, Ad ad)
        {
            _helper.Navigate("http://www2.kapaza.be/nl/ai/form/0");
            _kapazaHelper.TrackManualImageUploadsBegin(md5 => { md5File = md5; }, "gif");
            await _helper.WaitForFinishAsync("kapaza.be/nl/ai/form");
            
            _helper.InvokeScriptString("$(\"button[name='validate']\").html('adscan save')");

            var elements = await ScanPage();
            ad.Pages.Add(new Page());
            ad.Pages.Last().Name = "page1";
            ad.Pages.Last().Url = "http://www2.kapaza.be/ai/form/";
            ad.Pages.Last().Elements.AddRange(elements);
            ad.Account = account;
            _kapazaHelper.TrackManualImageUploadsEnd();
            _kapazaHelper.CleanTemporaryImages();
            return true;
        }



        public async Task<DeleteResult> DeleteAsync(Account account, Ad ad, bool guessMode)
        {

            var id = ad.LastPlacedURL;

            if (String.IsNullOrEmpty(id))
            {
              //  return DeleteResult.Deleted;
                id = Title;
            }
                
            _helper.Navigate("http://www2.kapaza.be/store/account/0");


            await _helper.WaitForFinishAsync("store/account/0");
            await _kapazaHelper.WaitUntilExistsAsync("$(\"input.form-control[name='q']\")");
            await _helper.InvokeScriptStringAsync(String.Format("$(\"input.form-control[name='q']\").val('{0}')", id.EscapeTextBox()), true);

            await _helper.InvokeScriptStringAsync("$(\"button.search-button\").click()", true);
            await _helper.WaitForFinishAsync();
            // nog maar een x
            //todo: waarom moeten we dit nog een x doen?
            await _helper.InvokeScriptStringAsync("$(\"button.search-button\").click()", true);
            await _helper.WaitForFinishAsync();


            var pendingSelector = String.Format("$(\"#store_pending_list strong:textEquals('{0}')\").html()", id.EscapeTextBox());
            if (_helper.InvokeScriptString(pendingSelector).Equals(id))
            {
                //staat in de wachtrij
                return DeleteResult.Failed;
            }
            var selector = string.Format("$(\"div.listing_adtitle a:textEquals('{0}')\")", id.EscapeTextBox());
            _log.Trace(_helper.InvokeScriptString(selector + ".html()"));


            if (_helper.InvokeScriptString(selector + ".html()").Contains("undefined"))
            {
                return DeleteResult.NotFound;
            }
            else
            {
                _helper.InvokeScriptString(selector + ".parent().parent().find(\"input.ad_selection\").click()");
                Trace.WriteLine(selector + ".parent().parent().find(\"input.ad_selection\").click()");
                await TaskEx.Delay(200,_cancelToken);
                _helper.InvokeScriptString(selector + ".parent().parent().find(\"div.actions_edit_delete a.delete\").click()");
                await _helper.WaitForFinishAsync();
                if (_kapazaHelper.InvokeScriptString("$(\"#udr_1\")").Equals("undefined"))
                {
                    return DeleteResult.Deleted;    
                }
                await _kapazaHelper.ClickAsync("$(\"#udr_1\")");
                await _kapazaHelper.ClickAsync("$(\"#valid_button\")");
                await _helper.WaitForFinishAsync();
                return DeleteResult.Deleted;
            }


            await TaskEx.Delay(1, _cancelToken);
            return DeleteResult.Deleted;
        }

        public async Task<bool> EditAsync(Account account, Ad ad, Action removeSplash)
        {
            await FillAsync(account, ad,true);
            removeSplash();
            _kapazaHelper.TrackManualImageUploadsBegin(md5 => { md5File = md5; }, "gif");
            var elements = await ScanPage();
            ad.Pages.Clear();
            ad.Pages.Add(new Page());
            ad.Pages.Last().Name = "page1";
            ad.Pages.Last().Url = "http://www2.kapaza.be/ai/form/";
            ad.Pages.Last().Elements.AddRange(elements);
            ad.Account = account;
            _kapazaHelper.TrackManualImageUploadsEnd();
            _kapazaHelper.CleanTemporaryImages();
            return true;
        }

        public PluginStatus Status { get; set; }
        #endregion

        #region "IPluginTemplateAsync"

        public void SetAd(Ad ad)
        {
            _ad = ad;
        }

        public string Title
        {
            get { return Regex.Replace(FindElement("INPUT", "id", "subject").Value, "\\s+", " "); }
            // get { return FindElement("INPUT", "id", "subject").Value; }
            set { FindElement("INPUT", "id", "subject").Value = value.TakeFirst(40); }
        }

        public string Description
        {
            get { return FindElement("TEXTAREA", "", "").Value; }
            set { FindElement("TEXTAREA", "", "").Value = value; }
        }
        public string Price
        {
            get { return FindElement("INPUT", "id", "price").Value; }
            set { FindElement("INPUT", "id", "price").Value = value; }
        }
        public string GetImage(int number)
        {
            return FindElement("image", "id", number.ToString()).Value;
        }

        public void SetImage(int number, string value)
        {
            FindElement("image", "id", number.ToString()).Value = value;
        }

        public string Extra { get; set; }

        public string BareAdName { get { return Title; } }

        private Element FindElement(string tagName, string attributeName, string lookForAttribute)
        {
            Element tempElement;
            if (string.IsNullOrEmpty(attributeName))
            {
                tempElement = (from x in _ad.FindPage("page1").Elements
                               where !string.IsNullOrEmpty(x.Tagname) && x.Tagname.Equals(tagName)
                               select x).SingleOrDefault();

            }
            else
            {
                List<Element> allElementsWithAttributeName = (from x in _ad.FindPage("page1").Elements where x.FindAttribute(attributeName) != null select x).ToList();
                tempElement = (from x in allElementsWithAttributeName where x.FindAttribute(attributeName).Value.Equals(lookForAttribute) select x).SingleOrDefault();
            }

            if (tempElement != null)
            {
                return tempElement;
            }
            var el = new Element { Tagname = tagName };
            el.Attributes.Add(new Attribute());
            el.Attributes.Last().Name = attributeName;
            el.Attributes.Last().Value = lookForAttribute;
            el.Attributes.Add(new Attribute());
            el.Attributes.Last().Name = "type";
            el.Attributes.Last().Value = "file";
            _ad.Pages.Last().Elements.Add(el);
            return el;
        }
        #endregion

        #region "Help"
        
        private async Task<Boolean> UploadImagesInAd(Ad ad)
        {
            DetectImageChanges();
            var images = (from x in ad.FindPage("page1").Elements
                          where (x.Tagname.Equals("image", StringComparison.InvariantCultureIgnoreCase)
                          && !x.Value.Equals("0") && !String.IsNullOrEmpty(x.Value))
                          select new { MD5 = x.Value, id = x.FindAttribute("id").Value });
            int num = 0;
            foreach (var image in images)
            {
                md5File = image.MD5;
                await _kapazaHelper.UploadImage(string.Format("$(\".qq-upload-button\")"), image.MD5, null, "gif", _tcsImageUploaded.Task);
                num++;
                await TaskEx.Delay(1000, _cancelToken);
            }
            return true;
        }

        async Task<List<Element>> ScanPage()
        {
            images.Clear();
            var tcsPageHook = new TaskCompletionSource<List<Element>>();
            _cancelToken.Register(tcsPageHook.SetCanceled);

            _helper.CreateJavascriptCallback("ScanPage", (method, args) =>
            {
                _log.Debug("Entering scanroutine");
                var test = _kapazaHelper.JSonToObject<List<Element>>(args[0].ToString());
                tcsPageHook.SetResult(test);
                return "";
            });

            DetectImageChanges();

            await _helper.InvokeScriptStringAsync(
                _helper.GetJavascript(Resources.ScanPage, "ScanPage"), true);


            var allElements = await tcsPageHook.Task;

            _cancelToken.ThrowIfCancellationRequested();

            foreach (var image in images)
            {
                var element = new Element { Tagname = "image" };
                element.Attributes.Add(new Attribute { Name = "id", Value = image.Key });
                element.Attributes.Add(new Attribute { Name = "type", Value = "file" });
                element.Attributes.Add(new Attribute { Name = "name", Value = image.Value });
                element.Value = image.Value;
                allElements.Add(element);
            }

            return allElements;

        }

        private void DetectImageChanges()
        {
            _tcsImageUploaded = new TaskCompletionSource<bool>();

            _helper.CreateJavascriptCallback("imageUploaded", (method, args) =>
            {
                _log.Debug("Entering scanroutine");
                images.Add(args[0].ToString(), md5File);
                _tcsImageUploaded.TrySetResult(true);
                _tcsImageUploaded = new TaskCompletionSource<bool>();
                return "";
            });

            _helper.InvokeScriptStringAsync(
                _helper.GetJavascript(Resources.ImageScripts, "UploadFinished"), true);
        }

        #endregion

    }
}
