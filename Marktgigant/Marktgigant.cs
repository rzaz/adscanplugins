﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.PeerToPeer.Collaboration;
using System.Threading;
using System.Threading.Tasks;
using Core;
using Core.My;
using Core.NavigationAsync;
using NLog;
using NLog.Targets;
using PluginFrameWork;
using PluginFrameWork.Ads;
using Attribute = PluginFrameWork.Ads.Attribute;

namespace Marktgigant
{
    public class Plugin : IPluginAsync, IPluginTemplateAsync

    {
        private Navigation _helper;
        private Helpers _marktgigantplazaHelper;
        private CancellationToken _cancelToken;
        private Ad _ad;
        private readonly Logger _log = LogManager.GetCurrentClassLogger();
        private string _md5File = "";

    #region "IPluginAsync"

        public CancellationToken CancelToken { set { _cancelToken = value; } }

        public string Name { get { return "Marktgigant"; } }

        public void Init(object webview)
        {
            _helper = new Navigation(webview, _cancelToken);
            _marktgigantplazaHelper = new Helpers(webview, _cancelToken);
            _helper.AutoInjectScripts();
        }

        public async Task<bool> LoginAsync(Account account)
        {
            _helper.Navigate("http://www.marktgigant.nl/?logintype=logout");
            await _helper.WaitForFinishAsync();
            _helper.Navigate("http://www.marktgigant.nl/login/");
            await _helper.WaitForFinishAsync();
            Cleanpage();
            await _marktgigantplazaHelper.WaitUntilExistsAsync(Helpers.CreateSelector("user"));
            await _marktgigantplazaHelper.SetTextBoxAsync(Helpers.CreateSelector("user"), account.Name);
            await _marktgigantplazaHelper.SetTextBoxAsync(Helpers.CreateSelector("pass"), account.Password);
            await _marktgigantplazaHelper.ClickAsync("$(\"input:submit[name='submit']\")");
            await _helper.WaitForFinishAsync(); //wachten tot we naar "uw advertenties" worden herleid
            return true;
        }
        
        //******************    so far ready
        
        public Task<bool> RecordAsync(Account account, Ad ad)
        {
            throw new NotImplementedException();
        }

        public Task<bool> EditAsync(Account account, Ad ad, Action removeSplash)
        {
            throw new NotImplementedException();
        }

        public Task<bool> FillAsync(Account account, Ad ad, bool catchExceptions)
        {
            throw new NotImplementedException();
        } 

        public Task<bool> ConfirmAdAsync(Ad ad, bool manualClick)
        {
            throw new NotImplementedException();
        }

        public Task<DeleteResult> DeleteAsync(Account account, Ad ad, bool guessMode)
        {
            throw new NotImplementedException();
        }
        
        public PluginStatus Status { get; set; }

    #endregion

    #region "IPluginTemplateAsync"

        public void SetAd(Ad ad)
        {
            _ad = ad;
        }

        public string Title
        {
            get { return FindElement("INPUT", "id", "products_name").Value; }
            set { FindElement("INPUT", "id", "products_name").Value = value; }
        }

        public string Price
        {
            get { return FindElement("INPUT", "id", "products_price").Value; }
            set { FindElement("INPUT", "id", "products_price").Value = value; }
        }
        
        public string Description
        {
            get { return FindElement("redactor", "", "").Value; }
            set { FindElement("redactor", "", "").Value = value; }
            // wel zorgen bij het opslaan in record dat het veld straks redactor heet 
        }
        
        public string GetImage(int number)
        {
            return FindElement("image", "id", number.ToString()).Value;
        }
        public void SetImage(int number, string value)
        {
            FindElement("image", "id", number.ToString()).Value = value;
        }

        public string Extra { get; set; }

        public string BareAdName { get { return Title; } }

        private Element FindElement(string tagName, string attributeName, string lookForAttribute)
        {
            Element tempElement;
            if (string.IsNullOrEmpty(attributeName))
            {
                tempElement = (from x in _ad.FindPage("page2").Elements
                               where !string.IsNullOrEmpty(x.Tagname) && x.Tagname.Equals(tagName)
                               select x).SingleOrDefault();

            }
            else
            {
                List<Element> allElementsWithAttributeName = (from x in _ad.FindPage("page2").Elements where x.FindAttribute(attributeName) != null select x).ToList();
                tempElement = (from x in allElementsWithAttributeName where x.FindAttribute(attributeName).Value.Equals(lookForAttribute) select x).SingleOrDefault();
            }

            if (tempElement != null)
            {
                return tempElement;
            }
            var el = new Element { Tagname = tagName };
            el.Attributes.Add(new Attribute());
            el.Attributes.Last().Name = attributeName;
            el.Attributes.Last().Value = lookForAttribute;
            el.Attributes.Add(new Attribute());
            el.Attributes.Last().Name = "type";
            el.Attributes.Last().Value = "file";
            _ad.Pages.Last().Elements.Add(el);
            return el;
        }
        
    #endregion

         void Cleanpage()
         {
             "masterhead".TojQueryString().SetConcat("hide").Invoke(_helper);
             JQueryString.Create().SetElement("div").SetClass("grid-row.ri3").SetConcat("remove").Invoke(_helper);
             JQueryString.Create().SetElement("div").SetClass("grid-row.ri4").SetConcat("remove").Invoke(_helper);
         }

    }






}
