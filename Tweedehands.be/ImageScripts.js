﻿//////////////////////////////////////////////
//$SNIPPET: UploadFinished
//////////////////////////////////////////////

var ImageId = 0;
var NeededImages = 0;
var Uploading = false;
var ImageNumbers = [0, 0, 0, 0, 0, 0, 0, 0];

function GetImageNr() {
    for (var i = 0; i < ImageNumbers.length; i++) {
        if (ImageNumbers[i] == 0) {
            return i + 1;
        }
    }
    return 0;
};

function RemoveImageNr(number) {
    ImageNumbers[number - 1] = 0;
    return 0;
};

//oke
$("body").on("click", "input:file", function (ev) {
    if (Uploading) {
        ev.preventDefault();
        ev.stopPropagation();
        return false;
    };
    Uploading = true;
    ImageId = GetImageNr();
    NeededImages = $("ul.preview-list img").length + 1;

    console.log('imageID: ' + ImageId);
    return true;
});

// alle changes behalve de body gaan nooit af!
$("body").change(function () {

    if (Uploading) {
        console.log('imageupload change neededpics :' + NeededImages);
        console.log('current pics :' + $("ul.preview-list img").length);

        window.checker = setInterval(function () {
            if ($("span.upload-error-message").length > 0) {
                //error ? krijg je bij twee keer zelfde plaatje laden
                console.log('sukkel dit plaatje had je al geupload');
                Uploading = false;
                clearInterval(window.checker);
            } else {
                if ($("ul.preview-list img").length == NeededImages) {
                    clearInterval(window.checker);
                    ImageNumbers[ImageId - 1] = 1;
                    //sla image nummer op in de parent
                    $("ul.preview-list img").last().parent().attr("image_id", ImageId);

                    if (Uploading) {
                        adscan.imageUploaded(ImageId);
                    }
                    Uploading = false;
                    console.log('image with id ' + ImageId + " uploaded");
                };
            }
        }, 200);
    }
});

//okee
$("div.syi-photos").on("click", "input:button", function () {
    var imageNumberToDelete = $(this).parent().attr("image_id");
    RemoveImageNr(imageNumberToDelete);
    adscan.imageDelete(ImageNumberToDelete);
    console.log("image delete : " + imageNumberToDelete);
});