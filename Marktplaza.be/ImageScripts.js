﻿//////////////////////////////////////////////
//$SNIPPET: UploadFinished
//////////////////////////////////////////////
var ImageId = 0;
var NeededPics = 0;
var uploading = false;

function GetDataNr() {
    // return first free data number
    var dataNumbers = [0, 0, 0, 0, 0, 0, 0, 0];

    $("[data-nr]").each(function () {
        var datanr = parseInt($(this).attr("data-nr"));
        dataNumbers[datanr - 1] = 1;
    });

    for (var i = 0; i < dataNumbers.length; i++) {
        if (dataNumbers[i] == 0) {
            return i + 1;
        }
    }
    return 0;
}

$("input:file").on("mousedown", function (ev) {
    if (uploading) {
        ev.preventDefault();
        ev.stopPropagation();
        return false;
    };
    uploading = true;
    ImageId = GetDataNr();
    NeededPics = $("#imageupload img").length + 1;
    //console.log('imageID: ' + ImageId );
})

$("#imageupload").change(function () {
    var item = $("#imageupload img").length;

    console.log('imageupload change neededpics :' + NeededPics);
    console.log('current pics :' + $("#imageupload img").length);

    window.checker = setInterval(function () {
        if ($("#imageupload img").length == NeededPics) {
            clearInterval(window.checker);
            console.log('image with id ' + ImageId);
            if (uploading) {
                adscan.imageUploaded(ImageId);
            }
            uploading = false;
        };
    }, 200);
});

$("div.garbage").click(function () {
    var dataNr = $(this).parent().attr("data-nr");
    adscan.imageDelete(dataNr);
    console.log("image delete : " + dataNr);
})
