﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Core;
using Core.NavigationAsync;
using Marktplaza.be.Properties;
using NLog;
using PluginFrameWork;
using PluginFrameWork.Ads;
using Attribute = PluginFrameWork.Ads.Attribute;

namespace Marktplaza.be
{
    public class Plugin : IPluginAsync, IPluginTemplateAsync
    {
        private Navigation _helper;
        private Helpers _marktplazaHelper;
        private CancellationToken _cancelToken;
        private Ad _ad;
        private readonly Logger _log = LogManager.GetCurrentClassLogger();
        private string _md5File = "";

        #region "IPluginAsync"
        Dictionary<string, string> images = new Dictionary<string, string>();
        private TaskCompletionSource<bool> _tcsImageUploaded;


        public string Name { get { return "Marktplaza.be"; } }
        public CancellationToken CancelToken { set { _cancelToken = value; } }

        public void Init(object webview)
        {
            _helper = new Navigation(webview, _cancelToken);
            _marktplazaHelper = new Helpers(webview, _cancelToken);
            _helper.AutoInjectScripts();
        }

        public async Task<bool> LoginAsync(Account account)
        {
            _helper.Navigate("http://www.Marktplaza.be/logout.html");
            await _helper.WaitForFinishAsync();
            _helper.Navigate("http://www.Marktplaza.be/login.html");
            await _helper.WaitForFinishAsync();
            _helper.InvokeScriptString("$('#header').hide()");

            await _marktplazaHelper.WaitUntilExistsAsync(Helpers.CreateSelector("login-gebruikersnaam"));
            await _marktplazaHelper.SetTextBoxAsync(Helpers.CreateSelector("login-gebruikersnaam"), account.Name);
            await _marktplazaHelper.SetTextBoxAsync(Helpers.CreateSelector("login-wachtwoord"), account.Password);
            await _marktplazaHelper.ClickAsync("$(\"button:submit[name='btnLogin']\")");
            await _helper.WaitForFinishAsync(); //wachten tot we naar "uw advertenties" worden herleid
            return true;
        }

        public async Task<bool> FillAsync(Account account, Ad ad, bool catchExceptions)
        {
            _helper.Navigate("http://www.Marktplaza.be/place.html");
            await _helper.WaitForFinishAsync("place");
            await TaskEx.Delay(200, _cancelToken);

            images.Clear();

            _helper.InvokeScriptString("$('#header').hide()");
            // eerste pagina


            var listitems = from Element x in ad.FindPage("page1").Elements where x.Tagname.Equals("LI") select x;

            foreach (var listitem in listitems)
            {
                var selector = Helpers.CreateSelector("LI", "data-nr", listitem.FindAttribute("data-nr").Value);
                await _helper.InvokeScriptStringAsync(selector + ".click()", true);
                //await _marktplazaHelper.ClickAsync(selector);
                _log.Debug("listitem " + listitem.FindAttribute("data-nr").Value);
                await TaskEx.Delay(1);

            }

            // tweede pagina
            await _helper.WaitForFinishAsync();
            _helper.InvokeScriptString("$('#header').hide()");

            await _helper.InvokeScriptStringAsync("$(\"#promo-tr-listtopad\").hide()", true);//remove rubriektopper
            await _helper.InvokeScriptStringAsync("$(\"#promo-tr-frontpage\").hide()", true);//remove homepagetopper
            await _helper.InvokeScriptStringAsync("$(\"#promo-tr-colourbar\").hide()", true);//remove opvallende achtergrond
            await _helper.InvokeScriptStringAsync("$(\"#promo-tr-superpromo\").hide()", true);//remove super promotie


            //select boxes
            var selects = from Element x in ad.FindPage("page2").Elements where x.Tagname.Equals("SELECT") select x;

            _log.Debug("selects");
            foreach (var selectbox in selects)
            {
                var controlId = selectbox.FindAttribute("id").Value;
                _log.Trace(controlId);
                _log.Trace("SetSelectboxAsync");
                await _marktplazaHelper.SetSelectboxAsync(Helpers.CreateSelector(controlId), selectbox.Value);
            }

            // radio's
            _cancelToken.ThrowIfCancellationRequested();
            _log.Debug("radios");

            foreach (var radioButton in Helpers.GetRadiosFromElements(ad.FindPage("page2").Elements))
            {
                await _marktplazaHelper.ClickRadioAsync(Helpers.CreateSelector("INPUT", "name", radioButton.FindAttribute("name").Value));
                _log.Debug("radio " + radioButton.FindAttribute("name").Value);
                await TaskEx.Delay(1);
            }

            //checkboxen
            _cancelToken.ThrowIfCancellationRequested();
            _log.Debug("checkboxen");
            foreach (var checkBox in Helpers.GetCheckboxesFromElements(ad.FindPage("page2").Elements))
            {
                if (checkBox.Value != "True" && checkBox.Value != "False") checkBox.Value = "False";
                //await _marktplazaHelper.ClickCheckboxAsync(Helpers.CreateSelector(checkBox.FindAttribute("id").Value)).TimeoutAfter(1000);

                await ClickCheckboxAsync(Helpers.CreateSelector(checkBox.FindAttribute("id").Value), Convert.ToBoolean(checkBox.Value)).TimeoutAfter(1000);


                _log.Debug("checkbox " + checkBox.FindAttribute("id").Value);
                await TaskEx.Delay(1);
            }

            //textboxen
            _cancelToken.ThrowIfCancellationRequested();
            _log.Debug("textboxen");
            foreach (var textBox in Helpers.GetTextBoxesFromElements(ad.FindPage("page2").Elements))
            {
                if (!String.IsNullOrEmpty(textBox.Value))
                {
                    await _marktplazaHelper.SetTextBoxAsync(Helpers.CreateSelector(textBox.FindAttribute("id").Value), textBox.Value.Replace("\\", "\\\\").Replace("'", "\\'")).TimeoutAfter(1000);
                    _log.Debug("textbox " + textBox.FindAttribute("id").Value);
                    await TaskEx.Delay(1);
                }
            }

            // tinymce
            var tinymce = (from x in ad.FindPage("page2").Elements where x.Tagname.Equals("tinymce") select x).Single();

            var htmlConverter = new HtmlConverter();
            var keeplist = new[] { "UL", "LI" }.ToList();
            var replacelist = new List<Translate> { new Translate("P", "P", false) };

            {
                htmlConverter.TagsToKeep = keeplist;
                htmlConverter.ReplaceList = replacelist;
                htmlConverter.HmtlString = tinymce.Value;
                htmlConverter.convert();

            }
            var html = htmlConverter.ConvertedHtml;
            html = html.Replace("</BR>", "<BR>");
            html = html.Replace("<BR/>", "<BR>");
            //html = html.Replace("\r\n", "<BR>'");
            //html = html.Replace("\r", "<BR>'");
            //html = html.Replace("\n", "<BR>'");

            html = html.Replace("\r\n", "");
            html = html.Replace("\r", "");
            html = html.Replace("\n", "");


            html = html.Replace("'", "\\'");

            while (_helper.InvokeScriptString("tinyMCE.activeEditor.getContent()").Equals(""))
            {
                await TaskEx.Delay(500);
                _cancelToken.ThrowIfCancellationRequested();
                var text = string.Format("tinyMCE.activeEditor.setContent('{0}')", html);
                _helper.InvokeScriptString(text);
                //html = Replace(html, Chr(13), "") 
            }

            // images
            await UploadImagesInAd(ad).TimeoutAfter(60000);

            return true;
        }

        public async Task<bool> ConfirmAdAsync(Ad ad, bool manualClick)
        {
            if (!manualClick)
            {
                _helper.InvokeScriptString("$(\"#testknop\").click();");
            }
            else
            {
                await _helper.InvokeScriptStringAsync("$(\"#testknop\").val('Plaats')", true);
                await _marktplazaHelper.ScrollIntoFocusAsync("$(\"#testknop\")");
                _helper.InvokeScriptString(String.Format("$(\"#testknop\").fadeTo({0}, 0.5).fadeTo({1}, 1.0).fadeTo({0}, 0.5).fadeTo({1}, 1.0).fadeTo({0}, 0.5).fadeTo({1}, 1.0).fadeTo({0}, 0.5).fadeTo({1}, 1.0).fadeTo({0}, 0.5).fadeTo({1}, 1.0)", 1500, 500));

            }
            await _helper.WaitForFinishAsync();
            _helper.InvokeScriptString("$('#header').hide()");

            //fouten ?
            if (_helper.InvokeScriptString("$(\"div.form-errors li\").length") != "0")
                throw new Exception("Error");

            // is dat ad succesvol geplaatst?
            var test = _helper.InvokeScriptString("$(\"div.placedonebox h2\")".Trim());
            _log.Trace(test.Equals("Advertentie geplaatst"));
            await TaskEx.Delay(5000, _cancelToken);

            // wat is het unieke id van deze ad ? (nodig bij deleten)
            var adurl = _helper.InvokeScriptString("$(\"ul.ad-share input:text\").val();".Trim());
            _ad.LastPlacedURL = adurl; // store ad id

            _ad.LastPlaced = DateTime.Now;
            return true;
        }

        public async Task<bool> RecordAsync(Account account, Ad ad)
        {
            _helper.Navigate("http://www.Marktplaza.be/place.html");
            await _helper.WaitForFinishAsync("place");
            await TaskEx.Delay(200, _cancelToken);

            images.Clear();

            _helper.InvokeScriptString("$('#header').hide()");
            //pagina 1 : rubriek keuze

            var elements = await ScanPage1();
            ad.Pages.Clear();
            ad.Pages.Add(new Page());
            ad.Pages.Last().Name = "page1";
            ad.Pages.Last().Url = _helper.InvokeScriptString("document.URL");
            ad.Pages.Last().Elements.AddRange(elements);
            ad.Account = account;

            //pagina 2 : de advertentie
            await _helper.WaitForFinishAsync(); //wachten op nog een redirect die ze doen anders schieten we het script op een verkeerde pagina in
            _helper.InvokeScriptString("$('#header').hide()"); //remove search bar
            await _helper.InvokeScriptStringAsync("$(\"#promo-tr-listtopad\").hide()", true);//remove rubriektopper
            await _helper.InvokeScriptStringAsync("$(\"#promo-tr-frontpage\").hide()", true);//remove homepagetopper
            await _helper.InvokeScriptStringAsync("$(\"#promo-tr-colourbar\").hide()", true);//remove opvallende achtergrond
            await _helper.InvokeScriptStringAsync("$(\"#promo-tr-superpromo\").hide()", true);//remove super promotie




            DetectImageChanges();

            _marktplazaHelper.TrackManualImageUploadsBegin(md5 => { _md5File = md5; }, "gif");
            elements = await ScanPage2();

            //todo Wat nu als je iets vergeten bent in te vullen zoals de titel???

            ad.Pages.Add(new Page());
            ad.Pages.Last().Name = "page2";
            ad.Pages.Last().Url = _helper.InvokeScriptString("document.URL");
            ad.Pages.Last().Elements.AddRange(elements);
            ad.Account = account;

            _marktplazaHelper.TrackManualImageUploadsEnd();
            _marktplazaHelper.CleanTemporaryImages();

            return true;
        }



        public int GetAdId(string url)
        {
            if (url == null)
            {
                url = "";
            }

            int startcut = url.LastIndexOf("-", StringComparison.Ordinal);
            int endcut = url.LastIndexOf(".", StringComparison.Ordinal);

            if (startcut > -1 && endcut > -1 && startcut < endcut)
            {
                string advertentieid = url.Substring(startcut + 1, endcut - startcut - 1);
                int num;
                int.TryParse(advertentieid, out num);
                return num;
            }
            _log.Debug("DELETE AD: Geen advertentie id?");
            return 0;
        }

        public async Task<DeleteResult> DeleteAsync(Account account, Ad ad, bool guessMode)
        {
            string url = ad.LastPlacedURL;

            if (String.IsNullOrEmpty(url))
            {
                return DeleteResult.NotFound;
            }

            url = url.Substring(url.LastIndexOf("/", System.StringComparison.Ordinal) + 1);
            var adId = Regex.Match(url, @"[0-9]+(?=\.html)").ToString();
            var title = Regex.Match(url, string.Format(@".+(?=-{0})", adId)).ToString().Replace("-", " ");

            _log.Debug("advertentie id " + adId);
            _helper.Navigate("http://www.marktplaza.be/user/list.html");

            // wacht op zoek box, vul in en druk enter
            await TaskEx.Delay(2000);
            await _marktplazaHelper.WaitUntilExistsAsync("$(\"#ads-q\")");
            await _helper.InvokeScriptStringAsync(String.Format("$(\"#ads-q\").val('{0}')", title), true);
            await _helper.InvokeScriptStringAsync("$(\"button:submit[name='btnSearch']\").click()", true);
            await _helper.WaitForFinishAsync();
            _helper.InvokeScriptString("$('#header').hide()");
            //'bestaat er checkbox met ons id 
            var selector = String.Format("$(\"input:checkbox[value='{0}']\").length", adId);
            int a = Int32.Parse(_helper.InvokeScriptString(selector));

            if (a != 0)
            {
                // klik ad checkbox
                selector = String.Format("$(\"input:checkbox[value='{0}']\").click()", adId);
                await _helper.InvokeScriptStringAsync(selector, true);
                // klik delete 
                await _helper.InvokeScriptStringAsync("$(\"button:submit[name='btnDelete']\").click()", true);
                //er komt een vraag of dit zeker weet, klik op ja
                await _helper.InvokeScriptStringAsync("$(\"button:submit[name='btnDeleteAdsYes']\").click()", true);
                //
                await _helper.WaitForFinishAsync();
                _helper.InvokeScriptString("$('#header').hide()");

                // zijn de ads verwijderd?
                var test = _helper.InvokeScriptString("$(\"p:contains('advertenties zijn verwijderd.')\").text()");

                if (test.Contains("undefined") | string.IsNullOrEmpty(test))
                {
                    _log.Debug("DELETE AD: verwijderen mislukt");
                    return DeleteResult.Failed;
                }
                _log.Debug("DELETE AD: advertentie is verwijderd");
                return DeleteResult.Deleted;
            }
            return DeleteResult.NotFound;
        }

        public async Task<bool> EditAsync(Account account, Ad ad, Action removeSplash)
        {
            await FillAsync(account, ad,true);
            removeSplash();
            
            _marktplazaHelper.TrackManualImageUploadsBegin(md5 => { _md5File = md5; }, "gif");
            var elements = await ScanPage2();

            ad.Pages.Remove(ad.Pages.Last()); // remove old page 2
            ad.Pages.Add(new Page());
            ad.Pages.Last().Name = "page2";
            ad.Pages.Last().Url = _helper.InvokeScriptString("document.URL");
            ad.Pages.Last().Elements.AddRange(elements);
            ad.Account = account;
            _marktplazaHelper.TrackManualImageUploadsEnd();
            _marktplazaHelper.CleanTemporaryImages();
            return true;
        }

        public PluginStatus Status { get; set; }
        #endregion

        #region "IPluginTemplateAsync"

        public void SetAd(Ad ad)
        {
            _ad = ad;
        }

        public string Title
        {
            get { return FindElement("INPUT", "id", "titel").Value; }
            set { FindElement("INPUT", "id", "titel").Value = value; }
        }

        public string Description
        {
            get { return FindElement("tinymce", "", "").Value; }
            set { FindElement("tinymce", "", "").Value = value; }
        }
        public string Price
        {
            get { return FindElement("INPUT", "id", "prijs").Value; }
            set { FindElement("INPUT", "id", "prijs").Value = value; }
        }
        public string GetImage(int number)
        {
            return FindElement("image", "id", number.ToString(CultureInfo.InvariantCulture)).Value;
        }

        public void SetImage(int number, string value)
        {
            FindElement("image", "id", number.ToString(CultureInfo.InvariantCulture)).Value = value;
        }

        public string Extra { get; set; }

        public string BareAdName { get { return Title; } }

        private Element FindElement(string tagName, string attributeName, string lookForAttribute)
        {
            Element tempElement;
            if (string.IsNullOrEmpty(attributeName))
            {
                tempElement = (from x in _ad.FindPage("page2").Elements
                               where !string.IsNullOrEmpty(x.Tagname) && x.Tagname.Equals(tagName)
                               select x).SingleOrDefault();

            }
            else
            {
                List<Element> allElementsWithAttributeName = (from x in _ad.FindPage("page2").Elements where x.FindAttribute(attributeName) != null && x.Tagname.Equals(tagName) select x).ToList();
                tempElement = (from x in allElementsWithAttributeName where x.FindAttribute(attributeName).Value.Equals(lookForAttribute) select x).SingleOrDefault();
            }

            if (tempElement != null)
            {
                return tempElement;
            }
            var el = new Element { Tagname = tagName };
            el.Attributes.Add(new Attribute());
            el.Attributes.Last().Name = attributeName;
            el.Attributes.Last().Value = lookForAttribute;
            el.Attributes.Add(new Attribute());
            el.Attributes.Last().Name = "type";
            el.Attributes.Last().Value = "file";
            _ad.Pages.Last().Elements.Add(el);
            return el;
        }
        #endregion


        private async Task<List<Element>> ScanPage1()
        {
            var tcsPageHook = new TaskCompletionSource<List<Element>>();
            _helper.CreateJavascriptCallback("ScanPage", (method, args) =>
            {
                _log.Debug("Entering scanroutine");
                var test = _marktplazaHelper.JSonToObject<List<Element>>(args[0].ToString());
                tcsPageHook.SetResult(test);
                return "";
            });

            await _helper.InvokeScriptStringAsync(
               _helper.GetJavascript(Resources.ScanPage1, "ScanPage1"), true);

            var allElements = await tcsPageHook.Task;
            return allElements;
        }
        private async Task<List<Element>> ScanPage2()
        {

            var tcsPageHook = new TaskCompletionSource<List<Element>>();

            _cancelToken.Register(tcsPageHook.SetCanceled);

            _helper.CreateJavascriptCallback("ScanPage", (method, args) =>
            {
                _log.Debug("Entering scanroutine");
                var test = _marktplazaHelper.JSonToObject<List<Element>>(args[0].ToString());
                tcsPageHook.SetResult(test);
                return "";
            });

            await _helper.InvokeScriptStringAsync(
                _helper.GetJavascript(Resources.ScanPage2, "ScanPage2"), true);

            var allElements = await tcsPageHook.Task;

            _cancelToken.ThrowIfCancellationRequested();

            foreach (var image in images)
            {
                var element = new Element { Tagname = "image" };
                element.Attributes.Add(new Attribute { Name = "id", Value = image.Key });
                element.Attributes.Add(new Attribute { Name = "type", Value = "file" });
                element.Attributes.Add(new Attribute { Name = "name", Value = image.Value });
                element.Value = image.Value;
                allElements.Add(element);
            }

            return allElements;
        }
        private async Task<Boolean> UploadImagesInAd(Ad ad)
        {
            DetectImageChanges();

            var imagelist = (from x in ad.FindPage("page2").Elements
                             where (x.Tagname.Equals("image", StringComparison.InvariantCultureIgnoreCase)
                             && !x.Value.Equals("0") && !String.IsNullOrEmpty(x.Value))
                             select new { MD5 = x.Value, id = x.FindAttribute("id").Value });

            foreach (var image in imagelist)
            {
                _md5File = image.MD5;
                await _marktplazaHelper.UploadImage(string.Format("$(\"input:file\").last()"), image.MD5, null, "gif", _tcsImageUploaded.Task);
                await TaskEx.Delay(1000, _cancelToken);
            }
            return true;
        }

        private async Task<Boolean> ClickCheckboxAsync(String selector, bool check)
        {
            var value = _marktplazaHelper.InvokeScriptString(selector + ".prop('checked')");
            if (value != check.ToString().ToLower())
            {
                return await _marktplazaHelper.ClickAsync(selector);
            }
            return true;
        }

        private void DetectImageChanges()
        {
            _tcsImageUploaded = new TaskCompletionSource<bool>();

            _helper.CreateJavascriptCallback("imageUploaded", (method, args) =>
            {
                _log.Debug("Entering scanroutine");
                images.Add(args[0].ToString(), _md5File);
                _tcsImageUploaded.TrySetResult(true);
                _tcsImageUploaded = new TaskCompletionSource<bool>();
                return "";
            });

            _helper.CreateJavascriptCallback("imageDelete", (method, args) =>
            {
                _log.Debug("Entering  image delete routine");
                _log.Debug(args[0].ToString());
                images.Remove(args[0].ToString());
                return "";
            });

            _helper.InvokeScriptStringAsync(
                _helper.GetJavascript(Resources.ImageScripts, "UploadFinished"), true);
        }
    }
}
