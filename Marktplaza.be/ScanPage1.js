﻿//////////////////////////////////////////////
//$SNIPPET: ScanPage1
//////////////////////////////////////////////
$("html").on("submit", function (ev) {
    console.log("submit page 1");
    var elements = [];
    $("li.active").each(function () {
        var element = {
            "Tagname": $(this).prop("tagName"),
            "Parent": null,
            "Value": $(this).val(),
            "Attributes": [{
                "Name": "type",
                "Value": $(this).prop("type")
            }, {
                "Name": "data-nr",
                "Value": $(this).attr("data-nr")
            }]
        }
        elements.push(element);
    });

    adscan.ScanPage(JSON.stringify(elements));
    return true;
});