﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using Core.NavigationAsync;
using NLog;
using PluginFrameWork;
using PluginFrameWork.Ads;
using Attribute = PluginFrameWork.Ads.Attribute;
using Helpers = Core.NavigationAsync.Marktplaats.Helpers;

namespace Marktplaats
{
    public class Plugin : IPluginAsync, IPluginTemplateAsync
    {
        private Navigation _helper;
        private Helpers _mpHelper;
        private CancellationToken _cancelToken;
        private Ad _ad;
        private readonly Logger _log = LogManager.GetCurrentClassLogger();

        #region "IPluginAsync"

        CancellationToken IPluginAsync.CancelToken
        {
            set { _cancelToken = value; }
        }

        async Task<DeleteResult> IPluginAsync.DeleteAsync(Account account, Ad ad, bool guessMode)
        {
            _cancelToken.ThrowIfCancellationRequested();

            var plugin = (IPluginTemplateAsync)this;
            //checken of we op basis van id gaan deleten of op basis van naam
            if (guessMode)
            {
                return await DeleteByTitleAsync(plugin.Title, -1);
            }

            //ad.LastPlacedURL = 791792438.ToString(CultureInfo.InvariantCulture);
            int id;
            //we hebben als het goed is het id opgeslagen in dit veld
            //nu proberen we het te parsen
            if (!int.TryParse(ad.LastPlacedURL, out id))
            {
                return DeleteResult.Failed;
            }
            _cancelToken.ThrowIfCancellationRequested();
            //we hebben dus een id, nu kijken of ie ook echt op marktplaats staat
            if (!await _mpHelper.AdExistsAsync(id))
            {
                return DeleteResult.NotFound;
            }
            _cancelToken.ThrowIfCancellationRequested();
            //de ad bestaat. Nu lezen we de titel zodat we straks via de searchbox 'm kunnen verwijderen
            //var title = _helper.InvokeScriptString("$(\"#title\").text()").Trim();
            //var title = "title".TojQueryString().SetConcat("text").Invoke(_helper);
            var title = JQueryString.Create("title").SetConcat("text").Invoke(_helper);
            return await DeleteByTitleAsync(title, id);
        }

        async Task<bool> IPluginAsync.EditAsync(Account account, Ad ad, Action removeSplash)
        {
            IPluginAsync instance = this;
            _cancelToken.ThrowIfCancellationRequested();
            await instance.FillAsync(account, ad,true);
            _cancelToken.ThrowIfCancellationRequested();
            var page2 = ad.FindPage("page2");
            ad.Pages.Remove(page2);
            removeSplash();
            await JQueryString.Create("syi-place-ad-button").SetInsert("span").SetConcat("text", "Update").InvokeAsync(_helper, true);
            //await _helper.InvokeScriptStringAsync("$(\"#syi-place-ad-button span\").text('Update')", true);
            await HookPage2(account, ad,fillImages);
            return true;
        }

        async Task<DeleteResult> DeleteByTitleAsync(string title, int thisIdMustExist)
        {
            _cancelToken.ThrowIfCancellationRequested();

            _helper.Navigate("https://marktplaats.nl/mymp/verkopen/index.html");
            await _helper.WaitForFinishAsync("verkopen/index.html");

            _cancelToken.ThrowIfCancellationRequested();

            await _mpHelper.SearchResultsAsync(title).TimeoutAfter(60000);

            _cancelToken.ThrowIfCancellationRequested();

            //als id gelijk is aan 0 of kleiner, dan hoeven we niet te checken op id
            if (thisIdMustExist <= 0)
            {
                //selecteer eerste searchresult
                if (!_mpHelper.SelectAdInSearchByTitle(title))
                {
                    return DeleteResult.NotFound;
                }
                _cancelToken.ThrowIfCancellationRequested();
            }
            else
            {
                if (_mpHelper.SearchResultExists(thisIdMustExist))
                {
                    _cancelToken.ThrowIfCancellationRequested();
                    //selecteer het id
                    if (!_mpHelper.SelectAdInSearchById(thisIdMustExist))
                    {
                        return DeleteResult.Failed;
                    }
                    _cancelToken.ThrowIfCancellationRequested();
                }
                else
                {
                    return DeleteResult.NotFound;
                }
            }

            _cancelToken.ThrowIfCancellationRequested();

            //deleteknop indrukken en checken of het gelukt is
            if (await _mpHelper.DeleteFromSearchAsync())
            {
                return DeleteResult.Deleted;
            }
            return DeleteResult.Failed;
        }

        private List<String> fillImages;
        async Task<bool> IPluginAsync.FillAsync(Account account, Ad ad, bool catchExceptions)
        {

            if (ad.FindPage("page2").Url.Equals("https://marktplaats.nl/syi/plaatsAdvertentie.html"))
            {
                //ga naar de plaats advertentie pagina
                _helper.Navigate("https://marktplaats.nl/syi/plaatsAdvertentie.html");
                _cancelToken.ThrowIfCancellationRequested();
                _log.Debug(Thread.CurrentThread.ManagedThreadId);
                await _helper.WaitForFinishAsync("syi/plaatsAdvertentie.html");
                await RemoveRubriekShortcuts();
                //todo is this isn't a task then it fails after the second attempt
                await TaskEx.Delay(1000, _cancelToken);

                //loop door de 2 of 3 listboxes en zet de tekst
                var page1 = ad.FindPage("page1");
                foreach (var element in page1.Elements)
                {
                    string id = element.Attributes.Find(x => x.Name.Equals("id")).Value;
                    string text = element.Attributes.Find(x => x.Name.Equals("text")).Value;
                    _mpHelper.ClickListItem(id, text);
                    await TaskEx.Delay(500, _cancelToken);
                }

                _cancelToken.ThrowIfCancellationRequested();

                //wacht eventueel op de nieuwe pagina als die er nog niet is
                if (!_helper.IsDocumentReady())
                {
                    await _helper.WaitForDocumentReady();
                }
                //blijft anders hangen in fullsize mode
                await TaskEx.Delay(2000, _cancelToken);
            }
            else
            {
                //we kunnen pagina1 skippen omdat we een volledige url hebben
                _cancelToken.ThrowIfCancellationRequested();
                _helper.Navigate(ad.FindPage("page2").Url);
                await _helper.WaitForFinishAsync();
            }

            _log.Debug("tekstvakken");
            //vul alle tekstvakken in
            _cancelToken.ThrowIfCancellationRequested();

            var page2 = ad.FindPage("page2");
            foreach (var textBox in Core.NavigationAsync.Helpers.GetTextBoxesFromElements(page2.Elements))
            {
                if (!textBox.FindAttribute("name").Value.Equals(""))
                {
                    await _mpHelper.SetTextBoxAsync(Core.NavigationAsync.Helpers.CreateSelector("INPUT", "name", textBox.FindAttribute("name").Value),
                                         textBox.Value.Replace("\\", "\\\\").Replace("'", "\\'")).CatchException(catchExceptions);
                }


            } _cancelToken.ThrowIfCancellationRequested();
            _log.Debug("radios");
            //vul alle radio buttons in
            foreach (var radioButton in Core.NavigationAsync.Helpers.GetRadiosFromElements(page2.Elements))
            {
                await _mpHelper.ClickRadioAsync(Core.NavigationAsync.Helpers.CreateSelector(radioButton.FindAttribute("id").Value)).CatchException(catchExceptions);
            }

            _cancelToken.ThrowIfCancellationRequested();
            _log.Debug("checkboxen");
            //vul alle checkboxes in
            foreach (var checkBox in Core.NavigationAsync.Helpers.GetCheckboxesFromElements(page2.Elements))
            {
                if (checkBox.FindAttribute("ignoreId")!=null)
                {
                    await _mpHelper.ClickCheckboxAsync(@"$(""#"" + $(""label:contains('" + checkBox.FindAttribute("text").Value + @"')"").attr('for') + """")").CatchException(catchExceptions);
                }
                else
                {
                    await _mpHelper.ClickCheckboxAsync(Core.NavigationAsync.Helpers.CreateSelector(checkBox.FindAttribute("id").Value)).CatchException(catchExceptions);    
                }
                
            }

            _cancelToken.ThrowIfCancellationRequested();
            _log.Debug("dropdowns");
            foreach (var select in Core.NavigationAsync.Helpers.GetSelectsFromElements(page2.Elements))
            {
                var id = select.FindAttribute("id").Value;
                var attr = select.FindAttribute("text");
                var selector = Core.NavigationAsync.Helpers.CreateSelector(id);

                if (attr != null)
                {
                    await _mpHelper.ClickSelectAsync(selector, attr.Value)
                                        .TimeoutAfter(5000, String.Format("Combo \"{0}\" with text \"{1}\" timeout", id, attr.Value)).CatchException(catchExceptions);
                }
                else
                {
                    await _mpHelper.ClickSelectIdAsync(selector, select.Value)
                                       .TimeoutAfter(5000, String.Format("Combo \"{0}\" with text \"{1}\" timeout", id, select.Value)).CatchException(catchExceptions);
                }
                    
            }
            _cancelToken.ThrowIfCancellationRequested();
            _log.Debug("omschrijving");
            //vul de omschrijving in
            var adText = Core.NavigationAsync.Helpers.GetAdText(page2.Elements);
            var fixedText = Regex.Replace(adText, "\\r|\\n", "", RegexOptions.Singleline);
            _mpHelper.InvokeScriptString("tinyMCE.activeEditor.setContent('" + fixedText + "')");


            //vul alle tabs in
            foreach (var select in Core.NavigationAsync.Helpers.GetTabs(page2.Elements))
            {
                await _mpHelper.ClickAsync(string.Format("$(\"a:contains('{0}')\")", select.Value)).CatchException(catchExceptions);
            }
            //Prijstab in oude bare-ads
            if (Core.NavigationAsync.Helpers.GetTabs(page2.Elements).Count == 0)
            {
                List<Element> allElementsWithAttributeName = (from x in page2.Elements where x.FindAttribute("id") != null select x).ToList();
                var temp = (from x in allElementsWithAttributeName where x.FindAttribute("id").Value.Equals("syi-price-type-dropdown") select x).SingleOrDefault();
                if (temp != null)
                {
                    _mpHelper.InvokeScriptString("$('.syi-price-tab').click()");
                }
            }

            //bieden vanaf invullen met de gewone prijs als ie staat aangevinkt en het tekstvak is leeg
            var bidScript = @"
if ($(""[value='minimum-bid-price']"").get(0).checked) {
	if (!$(""[name='price.minimumBidPrice']"").val()) {
		$(""[name='price.minimumBidPrice']"").val($(""[name='price.value']"").val())
	}
}
";
            _helper.InvokeScriptString(bidScript);

            var adMarkt = _helper.InvokeScriptString("$(\"#aurora-ad-type\").length");
            if (adMarkt.Equals("1"))
            {
                await _mpHelper.ClickAsync("$(\"#aurora-ad-type\")");
            }


            _mpHelper.InjectBlikvangerCatcher();

            int imageNr = 0;
            fillImages=new List<string>();
            foreach (var image in Core.NavigationAsync.Helpers.GetImages(page2.Elements))
            {
                _cancelToken.ThrowIfCancellationRequested();
                _log.Trace("plaatje");
                await _mpHelper.UploadImage(imageNr, image).TimeoutAfter(30000).CatchException(catchExceptions);
                imageNr++;
                fillImages.Add(image);
            }
            _cancelToken.ThrowIfCancellationRequested();
            return true;
        }

        async Task<bool> IPluginAsync.ConfirmAdAsync(Ad ad, bool manualClick)
        {

            _cancelToken.ThrowIfCancellationRequested();

            _log.Debug("confirm button click");
            if (!manualClick)
            {
                await _helper.InvokeScriptStringAsync("$(\"#syi-place-ad-button\").click();", true);
            }
            else
            {
                await _mpHelper.ScrollIntoFocusAsync("syi-place-ad-button");
                _helper.InvokeScriptString(
                    String.Format(
                        "$(\"#syi-place-ad-button\").fadeTo({0}, 0.5).fadeTo({1}, 1.0).fadeTo({0}, 0.5).fadeTo({1}, 1.0).fadeTo({0}, 0.5).fadeTo({1}, 1.0).fadeTo({0}, 0.5).fadeTo({1}, 1.0).fadeTo({0}, 0.5).fadeTo({1}, 1.0)", 1500, 500));

                await _helper.WaitForFinishAsync();
            }

            _cancelToken.ThrowIfCancellationRequested();

            await Verify();

            _log.Debug("lastplaced url");
            //BUG Dom not removeSplash
            ad.LastPlacedURL = (await _helper.InvokeScriptStringAsync("$(\"#vip-breadcrumbs-content\").find(\"span:contains('Advertentie')\").html()", true)).Replace("Advertentie ", "");
            ad.LastPlaced = DateTime.Now;
            return true;
        }

        async Task<bool> IPluginAsync.RecordAsync(Account account, Ad ad)
        {

            //Wachten op het kies rubriek scherm
            _cancelToken.ThrowIfCancellationRequested();
            _helper.Navigate("https://marktplaats.nl/syi/plaatsAdvertentie.html");
            await _helper.WaitForFinishAsync("syi/plaatsAdvertentie.html");
            await RemoveRubriekShortcuts();
            _cancelToken.ThrowIfCancellationRequested();

            //we zitten op pagina 1 en we wachten totdat alle
            //rubrieken etc zijn ingevuld.
            _cancelToken.ThrowIfCancellationRequested();
            var elements = await _mpHelper.WaitForPage1AndScan();
            _cancelToken.ThrowIfCancellationRequested();
            //voeg een eerste pagina toe
            //daarna de lijst met elementen die we hebben teruggekregen uit de hulpfunctie
            //aan de (nog lege) lijst toevoegen
            ad.Pages.Add(new Page());
            ad.Pages.Last().Name = "page1";
            ad.Pages.Last().Url = "https://marktplaats.nl/syi/plaatsAdvertentie.html";
            ad.Pages.Last().Elements.AddRange(elements);

            //wachten op pagina 2
            _cancelToken.ThrowIfCancellationRequested();
            await _helper.WaitForFinishAsync();
            _cancelToken.ThrowIfCancellationRequested();
            await _helper.InvokeScriptStringAsync("$(\"#syi-place-ad-button span\").text('Opslaan in Adscan')", true);
            await HookPage2(account, ad,null);
            _cancelToken.ThrowIfCancellationRequested();
            return true;
        }

        private async Task RemoveRubriekShortcuts()
        {
            await _helper.InvokeScriptStringAsync("$('#category-suggestion-form').parent().hide()", true);
            await _helper.InvokeScriptStringAsync("$('#syi-form').removeClass('hide-categories')", true);
        }

        private async Task HookPage2(Account account, Ad ad, List<String> images)
        {
            //Als er plaatjes geupload worden, klik dan automatisch de blikvanger weg
            _mpHelper.InjectBlikvangerCatcher();

            //hierin worden de geuploade plaatjes bijgehouden
            //het is het id en de md5 van het plaatje
            if (images == null)
                images = new List<String>();

            //hook het uploaden van de plaatjes om de collectie bij te houden
            _mpHelper.InjectImageHook((imageId, action) =>
            {
                switch (action)
                {
                    case "delete":
                        images.RemoveAt(int.Parse(Regex.Match(imageId, "[0-9]+").ToString())-1);
                        break;
                    default:
                        images.Add( action);
                        break;
                }
            });


            ad.Pages.Add(new Page());
            ad.Pages.Last().Name = "page2";
            ad.Pages.Last().Url = _mpHelper.InvokeScriptString("document.URL");
            List<Element> elements = await _mpHelper.WaitForPage2AndScan();
            //voeg nog de gekozen plaatjes als elementen toe
            foreach (var image in images)
            {
                var element = new Element { Tagname = "INPUT" };
                element.Attributes.Add(new Attribute { Name = "id", Value = "adscanImage-" + images.IndexOf(image).ToString()});
                element.Attributes.Add(new Attribute { Name = "type", Value = "file" });
                element.Attributes.Add(new Attribute { Name = "name", Value = image });
                element.Value = image;
                elements.Add(element);
            }
            ad.Pages.Last().Elements.AddRange(elements);
            ad.Account = account;
        }

        void IPluginAsync.Init(object webview)
        {
            //_mpHelper = null;
            //_helper = null;

            //MaxDelay = 15000;
            //MinDelay = 2000;
            //ManualPlace = true;
            //TimeSlotDays = 27;
            //_webview = webview;
            _helper = new Navigation(webview, _cancelToken);
            _mpHelper = new Helpers(webview, _cancelToken);
            //_helper.NoConflictModeJquery = false;
            //_helper.AutoInjectJquery();
            _helper.AutoInjectScripts();

            _helper.CreateJavascriptCallback("testing", (methodName, args) =>
            {
                Debugger.Break();
                return "hoihoihoi";
            });
            //_log.trace(_helper.GetJavascript(Resources.Scripts, "imagescript"));
            //webview.CertificateError += (s, e) =>
            //{
            //    e.Ignore = true;
            //    e.Handled = EventHandling.Modal;

            //};
        }

        Task<bool> IPluginAsync.LoginAsync(Account account)
        {
            _cancelToken.ThrowIfCancellationRequested();
            return _mpHelper.Login(account.Name, account.Password);
        }

        public string Name
        {
            get { return "Marktplaats"; }
        }

        public PluginStatus Status { get; set; }

        #endregion

        #region "IPluginTemplateAsync"

        void IPluginTemplateAsync.SetAd(Ad ad)
        {
            _ad = ad;
        }

        string IPluginTemplateAsync.Description
        {
            get { return FindElement("tinymce", "", "").Value; }
            set { FindElement("tinymce", "", "").Value = value; }
        }

        string IPluginTemplateAsync.Extra
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        string IPluginTemplateAsync.BareAdName
        {
            get
            {
                var elements = _ad.Pages[0].Elements;
                var returnText = String.Concat(elements[0].Attributes[1].Value, " > ",
                                               elements[1].Attributes[1].Value);
                if (_ad.Pages[0].Elements.Count > 2)
                    returnText += String.Concat(" | ", elements[2].Attributes[1].Value);

                return returnText;
            }
        }

        string IPluginTemplateAsync.GetImage(int number)
        {
            return FindElement("INPUT", "id", "adscanImage-" + number).Value;
        }

        void IPluginTemplateAsync.SetImage(int number, string value)
        {
            FindElement("INPUT", "id", "adscanImage-" + number).Value = value;
        }

        string IPluginTemplateAsync.Price
        {
            get { return FindElement("INPUT", "name", "price.value").Value; }
            set { FindElement("INPUT", "name", "price.value").Value = value; }
        }

        string IPluginTemplateAsync.Title
        {
            get { return Regex.Replace(FindElement("INPUT", "name", "title").Value,"\\s+" ," ");}
            set { FindElement("INPUT", "name", "title").Value = value.TakeFirst(60); }
        }

        private Element FindElement(string tagName, string attributeName, string lookForAttribute)
        {
            Element tempElement;
            if (string.IsNullOrEmpty(attributeName))
            {
                tempElement = (from x in _ad.FindPage("page2").Elements
                               where !string.IsNullOrEmpty(x.Tagname) && x.Tagname.Equals(tagName)
                               select x).SingleOrDefault();

            }
            else
            {
                List<Element> allElementsWithAttributeName = (from x in _ad.FindPage("page2").Elements where x.FindAttribute(attributeName) != null select x).ToList();
                tempElement = (from x in allElementsWithAttributeName where x.FindAttribute(attributeName).Value.Equals(lookForAttribute) select x).SingleOrDefault();
            }

            if (tempElement != null)
            {
                return tempElement;
            }
            var el = new Element { Tagname = tagName };
            if (lookForAttribute.Contains("adscanImage"))
            {
                el.Attributes.Add(new Attribute());
                el.Attributes.Last().Name = attributeName;
                el.Attributes.Last().Value = lookForAttribute;
                el.Attributes.Add(new Attribute());
                el.Attributes.Last().Name = "type";
                el.Attributes.Last().Value = "file";
                _ad.Pages.Last().Elements.Add(el);
            }
            return el;
        }

        #endregion

        private async Task<Boolean> Verify()
        {
            const string invalidScript = "$.map($(\"span[class^=invalid]\"), function(elem, index){ return $(elem).text();}).join(\"|\")";
            if ((await _helper.InvokeScriptStringAsync(invalidScript + ".length", true)).Equals("0")) return true;
            var errorText = await _helper.InvokeScriptStringAsync(invalidScript, true);
            throw new Exception(errorText);
        }


    }
}
