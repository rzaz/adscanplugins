﻿//////////////////////////////////////////////
//$SNIPPET: Imagescript
//////////////////////////////////////////////

var lastClickedImageId;
var uploading = false;

$("div.section-content").delegate("input:file", "click", function (ev) {
    if (uploading) {
        ev.preventDefault();
        ev.stopPropagation();
        return false;
    };
    uploading = true;
    fixImageIds();
    console.log('image ' + $(this).attr('adid') + ' clicked');
    lastClickedImageId = $(this).attr('adid');
    adscan.imageClick(lastClickedImageId);
})

$("div.section-content").delegate("a.remove", "mouseup", function () {
    fixImageIds();
    console.log('delete ' + $(this).parent().parent().find("input").not(":hidden").attr("adid") + ' clicked');
    adscan.imageDelete($(this).parent().parent().find("input").not(":hidden").attr("adid"));
})

function fixImageIds() {
    $("#syi-upload").find("input").not(":hidden").each(function (index) {
        $(this).attr('adid', 'adscanImage-' + (index + 1));
    });
}


$("#syi-upload").change(function () {
    console.log('waiting for ' + lastClickedImageId);
    var completed = $(this).find(".complete").length;
    window.checker = setInterval(function () {
        if ($("#syi-upload").find(".complete").length != completed) {
            clearInterval(window.checker);
            console.log('uploaded image with id ' + lastClickedImageId);
            fixImageIds();
            adscan.imageUploaded(lastClickedImageId);
            uploading = false;
        };
    }, 200);
});






//////////////////////////////////////////////
//$SNIPPET: jandoedel
//////////////////////////////////////////////


//////////////////////////////////////////////
///Dit is zomaar een testje
//////////////////////////////////////////////
// This example creates a rectangle based on the viewport
// on any 'zoom-changed' event.

function initialize() {

    var coachella = new window.google.maps.LatLng(33.6803003, -116.173894);
    var rectangle;

    var mapOptions = {
        zoom: 11,
        center: coachella,
        mapTypeId: window.google.maps.MapTypeId.TERRAIN
    };

    var map = new window.google.maps.Map(window.document.getElementById('map-canvas'),
        mapOptions);

    rectangle = new window.google.maps.Rectangle();

    window.google.maps.event.addListener(map, 'zoom_changed', function () {

        // Get the current bounds, which reflect the bounds before the zoom.
        var rectOptions = {
            strokeColor: '#FF0000',
            strokeOpacity: "{0}",
            strokeWeight: "{1}",
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            map: map,
            bounds: map.getBounds()
        };
        rectangle.setOptions(rectOptions);
    });
}

google.maps.event.addDomListener(window, 'load', initialize);

//////////////////////////////////////////////
///Dit is zomaar een testje
//////////////////////////////////////////////
//////////////////////////////////////////////
//$SNIPPET: BLA
//////////////////////////////////////////////

$("#ronald").on("click", "", function () {
    adscan.send("hoi");
});



//////////////////////////////////////////////
//$SNIPPET: BLA3
//////////////////////////////////////////////

var bla3;

