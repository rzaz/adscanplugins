﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Core;
using Core.NavigationAsync;
using NLog;
using PluginFrameWork;
using PluginFrameWork.Ads;
using Tweedehands.nl.Properties;
using Attribute = PluginFrameWork.Ads.Attribute;

namespace Tweedehands.nl
{
    public class Plugin : IPluginAsync, IPluginTemplateAsync
    {
        private Navigation _helper;
        private Helpers _tweedehandsHelper;
        private CancellationToken _cancelToken;
        private Ad _ad;
        private readonly Logger _log = LogManager.GetCurrentClassLogger();
        private string _md5File = "";

        #region "IPluginAsync"

        Dictionary<string, string> images = new Dictionary<string, string>();
        private TaskCompletionSource<bool> _tcsImageUploaded;

        public CancellationToken CancelToken { set { _cancelToken = value; } }

        public string Name { get { return "Tweedehands.nl"; } }

        public void Init(object webview)
        {
            _helper = new Navigation(webview, _cancelToken);
            _helper.AutoInjectJquery();
            _helper.AutoInjectScripts();
            _tweedehandsHelper = new Helpers(webview, _cancelToken);
        }

        public async Task<bool> LoginAsync(Account account)
        {

            _helper.Navigate("http://www.tweedehands.nl/loguit.html");
            await _helper.WaitForFinishAsync();

            _helper.Navigate("http://www.tweedehands.nl/login.html");
            await _helper.WaitForFinishAsync();


            var cookie = @"

var d = new Date();
    d.setTime(d.getTime() + (1000*24*60*60*1000));
    var expires = ""expires=""+d.toUTCString();
    document.cookie = ""luckynumber"" + ""="" + 0 + ""; "" + expires;
";
            await _helper.InvokeScriptStringAsync(cookie, false);

            //Cleanpage();

            //await _tweedehandsHelper.SetTextBoxAsync(Helpers.CreateSelector("email"), account.Name);
            await _tweedehandsHelper.SetTextBoxAsync((JQueryString)"email", account.Name);
            await _tweedehandsHelper.SetTextBoxAsync((JQueryString)"password", account.Password);

            //await _tweedehandsHelper.ClickAsync("$(\"button:submit.ui-button-secondary\")");
            
            await _tweedehandsHelper.ClickAsync(JQueryString.Create()
                                    .SetElement("button")
                                    .SetType("submit")
                                    .SetClass("ui-button-secondary"));

            await _helper.WaitForFinishAsync(); //wachten tot we naar "uw advertenties" worden herleid

            await TaskEx.Delay(1, _cancelToken);
            return true;
        }

        public async Task<bool> RecordAsync(Account account, Ad ad)
        {
            _helper.Navigate("http://www.tweedehands.nl/plaats/");
            await _helper.WaitForFinishAsync("plaats");
            await TaskEx.Delay(200, _cancelToken);

            images.Clear();


            //Probeer de snelrubrieken te verbergen.
            //Werkt nog niet helemaal goed!!!
            await _helper.InvokeScriptStringAsync(
            @"$('#page').on('DOMSubtreeModified', function() 
                                                  {
                                                    $(this).find('.suggestion-option').hide();
                                                  }
                           );"
            , true);


            //pagina 1 : er is maar 1 pagina!
            DetectImageChanges();
            _tweedehandsHelper.TrackManualImageUploadsBegin(md5 => { _md5File = md5; }, "gif");

            var elements = await ScanPage1();
            ad.Pages.Clear();
            ad.Pages.Add(new Page());
            ad.Pages.Last().Name = "page1";
            ad.Pages.Last().Url = _helper.InvokeScriptString("document.URL");
            ad.Pages.Last().Elements.AddRange(elements);
            ad.Account = account;

            //todo Wat nu als je iets vergeten bent in te vullen zoals de titel???

            _tweedehandsHelper.TrackManualImageUploadsEnd();
            _tweedehandsHelper.CleanTemporaryImages();

            return true;
        }

        public async Task<bool> FillAsync(Account account, Ad ad, bool catchExceptions)
        {
            images.Clear();
            _log.Debug("Wait for {0}", "http://www.tweedehands.nl/plaats/");
            _helper.Navigate("http://www.tweedehands.nl/plaats/");
            await _helper.WaitForFinishAsync("plaats");


            // click handmatig rubriek selecteren
            await _tweedehandsHelper.ClickRadioAsync(Helpers.CreateSelector("INPUT", "id", "manual-option"));

            //select boxes ==================================================================
            var selects = from Element x in ad.FindPage("page1").Elements where x.Tagname.Equals("SELECT") select x;
            _log.Debug("selects");
            foreach (var selectbox in selects)
            {
                var controlId = selectbox.FindAttribute("id").Value;
                _log.Trace("SetSelectboxAsync");
                Trace.WriteLine("selectbox Id : " + controlId + " Value : " + selectbox.Value);
                await _tweedehandsHelper.SetSelectboxAsync(Helpers.CreateSelector(controlId), selectbox.Value);
                await TaskEx.Delay(1);
            }
            _cancelToken.ThrowIfCancellationRequested();
            //await TaskEx.Delay(10000, _cancelToken);

            //text boxes ==================================================================
            _cancelToken.ThrowIfCancellationRequested();
            _log.Debug("textboxen");
            foreach (var textBox in Helpers.GetTextBoxesFromElements(ad.FindPage("page1").Elements))
            {
                if (!String.IsNullOrEmpty(textBox.Value))
                {
                    await _tweedehandsHelper.SetTextBoxAsync(Helpers.CreateSelector(textBox.FindAttribute("id").Value), textBox.Value.Replace("\\", "\\\\").Replace("'", "\\'")).TimeoutAfter(1000);
                    _log.Debug("textbox " + textBox.FindAttribute("id").Value);
                    await TaskEx.Delay(1);
                }
            }
            //await TaskEx.Delay(10000, _cancelToken);
            //radio boxes ==================================================================
            _log.Debug("radios");
            foreach (var radioButton in Helpers.GetRadiosFromElements(ad.FindPage("page1").Elements).Where((x) => !x.FindAttribute("id").Value.Equals("manual-option")))
            {
                await _tweedehandsHelper.ClickRadioAsync(Helpers.CreateSelector("INPUT", "name", radioButton.FindAttribute("name").Value));
                _log.Debug("radio " + radioButton.FindAttribute("name").Value);
                await TaskEx.Delay(1);
            }
            //await TaskEx.Delay(10000, _cancelToken);
            //checkboxen ==================================================================
            _cancelToken.ThrowIfCancellationRequested();
            _log.Debug("checkboxen");
            //vul alle checkboxes in
            foreach (var checkBox in Helpers.GetCheckboxesFromElements(ad.FindPage("page1").Elements))
            {
                if (checkBox.Value != "True" && checkBox.Value != "False") checkBox.Value = "False";
                bool ischecked = checkBox.Value == "True";
                await ClickCheckboxAsync(Helpers.CreateSelector(checkBox.FindAttribute("id").Value), ischecked);

                _log.Debug("checkbox " + checkBox.FindAttribute("id").Value + " checked: " + ischecked.ToString());
                Trace.WriteLine("checkbox " + checkBox.FindAttribute("id").Value + " checked: " + ischecked.ToString());
                await TaskEx.Delay(1,_cancelToken);
            }
            //await TaskEx.Delay(10000, _cancelToken);
            //text area ==================================================================
            var area = (from x in ad.FindPage("page1").Elements where x.Tagname.Equals("TEXTAREA") select x).Single();
            var id = area.FindAttribute("id").Value;
            var areatext = area.Value;
            var htmlConverter = new HtmlConverter();
            var keeplist = new List<string> { "UL", "LI" };
            var replacelist = new List<Translate> { new Translate("P", "P", false), new Translate("BR", "BR", false) };

            {
                htmlConverter.TagsToKeep = keeplist;
                htmlConverter.ReplaceList = replacelist;
                htmlConverter.HmtlString = areatext;
                htmlConverter.IgnoreStyles = true;
                htmlConverter.convert();
            }
            string html = htmlConverter.ConvertedHtml;
            html = html.Replace("\r", "");
            html = html.Replace("\n", "\\n");
            html = html.HtmlDecode();
            html = html.EscapeTextBox();
            
            html = string.Format("$(\"#{0}\").html('{1}')", id,html);
            /*
            html = string.Format(@"$(""#{0}"").html('{1}')", id,html);
            html = string.Format(@"$('#{0}').html('{1}')", id,html);
             */
            _tweedehandsHelper.InvokeScriptString(html);

            //images ==================================================================

            await UploadImagesInAd(ad).TimeoutAfter(60000);

            return true;
        }
        

        //******************    so far ready

        public async Task<bool> EditAsync(Account account, Ad ad, Action removeSplash)
        {
            await FillAsync(account, ad,true);

            //na het invullen van de pagina verwijderen we het splashscreen zodat de user kan wijzigen
            removeSplash();

            //nu gaan we wachten totdat de savebutton geklikt is
            //daarna verwijderen we de oude 'page' en kennen de nieuwe toe
            DetectImageChanges();
            _tweedehandsHelper.TrackManualImageUploadsBegin(md5 => { _md5File = md5; }, "gif");

            var elements = await ScanPage1();
            ad.Pages.Clear();
            ad.Pages.Add(new Page());
            ad.Pages.Last().Name = "page1";
            ad.Pages.Last().Url = _helper.InvokeScriptString("document.URL");
            ad.Pages.Last().Elements.AddRange(elements);
            ad.Account = account;

            _tweedehandsHelper.TrackManualImageUploadsEnd();
            _tweedehandsHelper.CleanTemporaryImages();

            return true;
        }


        public async Task<bool> ConfirmAdAsync(Ad ad, bool manualClick)
        {
            await _tweedehandsHelper.ClickAsync("$('.submit-form-button')");
            await _helper.WaitForFinishAsync();

            _ad.LastPlaced = DateTime.Now;
            _ad.LastPlacedURL = (await _helper.InvokeScriptStringAsync("document.URL", true));
            _ad.LastPlacedURL = Regex.Match(_ad.LastPlacedURL, @".+(?=\?plaats)").ToString();
            
            return true;
        }

        public async Task<DeleteResult> DeleteAsync(Account account, Ad ad, bool guessMode)
        {
           if (!guessMode)
            {
                if (_ad.LastPlacedURL == null)
                {
                    return DeleteResult.NotFound;
                }
                var url = Regex.Match(_ad.LastPlacedURL, @"(?-)[0-9]+(?=\.html)").ToString();
                url = String.Format("http://www.tweedehands.nl/beheer/{0}/verwijder/", url);
                _helper.Navigate(url);
                await _helper.WaitForFinishAsync();
                await TaskEx.Delay(2000, _cancelToken);
                var test2 = await _helper.InvokeScriptStringAsync("$('span.active-breadcrumb-content').html()", true);
                if (test2.Contains("geen toegang"))
                {
                    return DeleteResult.NotFound;
                }
    
            }
            else
            {
                if (_ad.LastPlacedURL == null)
                {
                    return DeleteResult.NotFound;
                }
                var url = Regex.Match(_ad.LastPlacedURL, @"(?-)[0-9]+(?=\.html)").ToString();
                url = String.Format("http://www.tweedehands.nl/beheer/{0}/verwijder/", url);
                _helper.Navigate(url);
                await _helper.WaitForFinishAsync();
                await TaskEx.Delay(2000, _cancelToken);
                var test2 = await _helper.InvokeScriptStringAsync("$('span.active-breadcrumb-content').html()", true);
                if (test2.Contains("geen toegang"))
                {
                    return DeleteResult.NotFound;
                }

                _helper.Navigate("http://www.tweedehands.nl/beheer/");
                await _helper.WaitForFinishAsync();
                await _tweedehandsHelper.SetTextBoxAsync("$('zoek')",Title);
                await TaskEx.Delay(10000);
            }

            
            await _tweedehandsHelper.ClickAsync("$('#delete_confirm')");
            await _helper.WaitForFinishAsync();
            
            return DeleteResult.Deleted;
        }

        public PluginStatus Status { get; set; }

        #endregion

        #region "IPluginTemplateAsync"

        public void SetAd(Ad ad)
        {
            _ad = ad;
        }

        public string Title
        {
            get { return FindElement("INPUT", "id", "title").Value; }
            set { FindElement("INPUT", "id", "title").Value = value.TakeFirst(50); }
        }

        public string Price
        {
            get { return FindElement("INPUT", "id", "bedragplaats_prijs_waarde").Value; }
            set { FindElement("INPUT", "id", "bedragplaats_prijs_waarde").Value = value; }
        }

        public string Description
        {
            get
            {
                return FindElement("textarea", "id", "description").Value;
            }
            set { FindElement("textarea", "id", "description").Value = value; }
        }

        public string GetImage(int number)
        {
            return FindElement("image", "id", number.ToString()).Value;
        }
        public void SetImage(int number, string value)
        {
            FindElement("image", "id", number.ToString()).Value = value;
        }

        public string Extra { get; set; }

        public string BareAdName { get { return Title; } }

        private Element FindElement(string tagName, string attributeName, string lookForAttribute)
        {
            Element tempElement;
            if (string.IsNullOrEmpty(attributeName))
            {
                tempElement = (from x in _ad.FindPage("page1").Elements
                               where !string.IsNullOrEmpty(x.Tagname) && x.Tagname.Equals(tagName)
                               select x).SingleOrDefault();

            }
            else
            {
                List<Element> allElementsWithAttributeName = (from x in _ad.FindPage("page1").Elements where x.FindAttribute(attributeName) != null select x).ToList();
                tempElement = (from x in allElementsWithAttributeName where x.FindAttribute(attributeName).Value.Equals(lookForAttribute) select x).SingleOrDefault();
            }

            if (tempElement != null)
            {
                return tempElement;
            }
            var el = new Element { Tagname = tagName };
            el.Attributes.Add(new Attribute());
            el.Attributes.Last().Name = attributeName;
            el.Attributes.Last().Value = lookForAttribute;
            el.Attributes.Add(new Attribute());
            el.Attributes.Last().Name = "type";
            el.Attributes.Last().Value = "file";
            _ad.Pages.Last().Elements.Add(el);
            return el;
        }

        #endregion

        void Cleanpage()
        {
            "header".TojQueryString().SetConcat("remove").Invoke(_helper);
            JQueryString.Create().SetElement("div").SetClass("comp-afdelingen-menu").SetConcat("remove").Invoke(_helper);
            JQueryString.Create().SetElement("div").SetClass("comp-statistics").SetConcat("remove").Invoke(_helper);
        }

        private async Task<List<Element>> ScanPage1()
        {
            var tcsPageHook = new TaskCompletionSource<List<Element>>();
            //var cancelMonitor  = Task.Factory.StartNew(()=>_helper.MonitorCancellation(_cancelToken,tcsPageHook.Task),_cancelToken);
            _cancelToken.Register(tcsPageHook.SetCanceled);
            _helper.CreateJavascriptCallback("ScanPage", (method, args) =>
            {
                _log.Debug("Entering scanroutine");
                var test = _tweedehandsHelper.JSonToObject<List<Element>>(args[0].ToString());
                tcsPageHook.SetResult(test);
                return "";
            });

            await _helper.InvokeScriptStringAsync(
               _helper.GetJavascript(Resources.ScanPage1, "ScanPage1"), true);

            await tcsPageHook.Task;
            _cancelToken.ThrowIfCancellationRequested();
            var allElements = tcsPageHook.Task.Result;

            foreach (var image in images)
            {
                var element = new Element { Tagname = "image" };
                element.Attributes.Add(new Attribute { Name = "id", Value = image.Key });
                element.Attributes.Add(new Attribute { Name = "type", Value = "file" });
                element.Attributes.Add(new Attribute { Name = "name", Value = image.Value });
                element.Value = image.Value;
                allElements.Add(element);
            }

            return allElements;
        }


        private async Task<Boolean> UploadImagesInAd(Ad ad)
        {
            DetectImageChanges();
            var images = (from x in ad.FindPage("page1").Elements
                          where (x.Tagname.Equals("image", StringComparison.InvariantCultureIgnoreCase)
                          && !x.Value.Equals("0") && !String.IsNullOrEmpty(x.Value))
                          select new { MD5 = x.Value, id = x.FindAttribute("id").Value });

            int num = 0;
            foreach (var image in images)
            {
                _md5File = image.MD5;
                await _tweedehandsHelper.UploadImage(string.Format("$(\"li.photo-input\")"), image.MD5, null, "jpg", _tcsImageUploaded.Task);
                num++;
                await TaskEx.Delay(1000, _cancelToken);
            }
            return true;
        }

        private void DetectImageChanges()
        {
            _tcsImageUploaded = new TaskCompletionSource<bool>();

            _helper.CreateJavascriptCallback("imageUploaded", (method, args) =>
            {
                _log.Debug("Entering scanroutine");
                images.Add(args[0].ToString(), _md5File);
                _tcsImageUploaded.TrySetResult(true);
                _tcsImageUploaded = new TaskCompletionSource<bool>();
                return "";
            });

            _helper.CreateJavascriptCallback("imageDelete", (method, args) =>
            {
                _log.Debug("Entering  image delete routine");
                _log.Debug(args[0].ToString());
                images.Remove(args[0].ToString());
                return "";
            });

            _helper.InvokeScriptStringAsync(
                _helper.GetJavascript(Resources.ImageScripts, "UploadFinished"), true);
        }

        private async Task<Boolean> ClickCheckboxAsync(String selector, bool check)
        {
            var value = _tweedehandsHelper.InvokeScriptString(selector + ".prop('checked')");
            if (value != check.ToString().ToLower())
            {
                return await _tweedehandsHelper.ClickAsync(selector);
            }
            return true;
        }


    }


}
