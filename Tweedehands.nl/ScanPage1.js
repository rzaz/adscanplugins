﻿//////////////////////////////////////////////
//$SNIPPET: ScanPage1
//////////////////////////////////////////////

$("button.submit-form-button.ui-button-primary.icon-punaise").html('Adscan save<span class="extra-text" data-extra-text="">en afrekenen</span>');

$("form.sell-your-item-form.main").submit(function (ev) {
    var elements = [];
    $("form.sell-your-item-form.main select").not(":hidden").each(function () {
        if ($(this).val() != "") {
            var element = {
                "Tagname": $(this).prop("tagName"),
                "Parent": null,
                "Value": $(this).val(),
                "Attributes": [
                    {
                        "Name": "id",
                        "Value": $(this).prop("id")
                    }, {
                        "Name": "type",
                        "Value": $(this).prop("type")
                    }, {
                        "Name": "name",
                        "Value": $(this).prop("name")
                    }, {
                        "Name": "text",
                        "Value": $(this).children("option:selected").text().trim()
                    }
                ]
            }
            elements.push(element);
        }
    });

    $("form.sell-your-item-form.main input:text").not(":hidden").each(function() {
        var element = {
            "Tagname": $(this).prop("tagName"),
            "Parent": null,
            "Value": $(this).val(),
            "Attributes": [
                {
                    "Name": "id",
                    "Value": $(this).prop("id")
                }, {
                    "Name": "type",
                    "Value": $(this).prop("type")
                }, {
                    "Name": "name",
                    "Value": $(this).prop("name")
                }
            ]
        }
        elements.push(element);
    });

    $("form.sell-your-item-form.main input:radio:checked").not(":hidden").each(function() {
        var element = {
            "Tagname": $(this).prop("tagName"),
            "Parent": null,
            "Value": 1,
            "Attributes": [
                {
                    "Name": "id",
                    "Value": $(this).prop("id")
                }, {
                    "Name": "type",
                    "Value": $(this).prop("type")
                }, {
                    "Name": "name",
                    "Value": $(this).prop("name")
                }
            ]
        }
        elements.push(element);
    });

    $("form.sell-your-item-form.main input:checkbox").not(":hidden").each(function () {
        var element = {
            "Tagname": $(this).prop("tagName"),
            "Parent": null,
            "Value": $(this).prop("checked"),
            "Attributes": [{
                "Name": "id",
                "Value": $(this).prop("id")
            }, {
                "Name": "type",
                "Value": $(this).prop("type")
            }, {
                "Name": "name",
                "Value": $(this).prop("name")
            }]
        }
        elements.push(element);
    });

    var element = {
        "Tagname": $("form.sell-your-item-form.main textarea").prop("tagName"),
        "Parent": null,
        "Value": $("form.sell-your-item-form.main textarea").val(),
        "Attributes": [{
            "Name": "id",
            "Value": $("form.sell-your-item-form.main textarea").prop("id")
        }]
    }
    elements.push(element);


    
    adscan.ScanPage(JSON.stringify(elements));
    ev.preventDefault();
    ev.stopPropagation();
    return false;
});